package io.gitee.mingbaobaba.security.example.exception;

import io.gitee.mingbaobaba.security.core.exception.SecurityBaseException;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * <p>异常处理</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/12 15:37
 */
@Component
public class SecurityHandlerExceptionResolver implements HandlerExceptionResolver {


    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        if (ex instanceof SecurityBaseException) {
            ModelAndView modelAndView = new ModelAndView();
            modelAndView.addObject("errorMsg", ex.getMessage());
            modelAndView.setViewName("/error");
            return modelAndView;
        }
        return null;
    }
}
