package io.gitee.mingbaobaba.security.example.repository;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Application;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2ApplicationRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>查询应用信息示例</p>
 * <p>
 * <p>
 * 基于redis方式必须实现此接口
 * <p>
 * 如果引入的是jpa则不需要，jpa的方式只需要在表security_oauth2_application配置即可
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/15 8:54
 */
@Repository
public class ExampleQueryApplication implements SecurityOauth2ApplicationRepository {
    @Override
    public SecurityOauth2Application getOauth2ApplicationByClientId(String clientId) {
        return SecurityOauth2Application.builder()
                .clientId("test")
                .clientName("测试客户端")
                .clientSecret("123456")
                .grantType("authorization_code,implicit,password,client_credentials")
                .scope("all")
                .redirectUri("http://www.baidu.com")
                .build();
    }
}
