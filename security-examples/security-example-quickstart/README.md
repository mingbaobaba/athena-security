### 快速使用示例

1. 启动示例工程
2. 浏览器访问地址 http://localhost:18081/securityLogin 
3. 输入配置文件配置的用户名密码，示例中我们配置的是 admin 123456 ，如果未配置则使用默认 admin admin@123
4. 登录成功会跳转到会跳转到配置文件配置的成功页面，示例中我们配置的是百度的页面，如未配置则跳转到默认的成功页
5. 如果配置了第三方的页面，那么登录成功后会在第三方的页面携带一个ticket的参数，可以用此参数调用/getTokenByTicket接口来换取登录的token值，
拿到token后在请求头添加 Athena-Authorization：token值 即可访问受保护的资源
