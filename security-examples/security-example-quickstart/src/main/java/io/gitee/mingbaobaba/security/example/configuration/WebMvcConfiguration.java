package io.gitee.mingbaobaba.security.example.configuration;

import io.gitee.mingbaobaba.security.core.router.SecurityRouter;
import io.gitee.mingbaobaba.security.starter.interceptor.SecurityInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>web配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/29 17:05
 */
@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new SecurityInterceptor(m -> SecurityRouter.build().run(m)))
                .addPathPatterns("/**");
    }

}
