package io.gitee.mingbaobaba.security.example.controller;

import io.gitee.mingbaobaba.security.core.annotion.SecurityCheckPermission;
import io.gitee.mingbaobaba.security.core.annotion.SecurityCheckRole;
import io.gitee.mingbaobaba.security.core.annotion.SecurityIgnore;
import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecurityPagination;
import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.enums.SecurityConditionType;
import io.gitee.mingbaobaba.security.core.utils.CaptchaUtil;
import io.gitee.mingbaobaba.security.core.utils.SecurityUtil;
import io.gitee.mingbaobaba.security.example.domain.User;
import lombok.SneakyThrows;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

/**
 * <p>测试controller</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 16:17
 */
@RestController
public class TestController {

    /**
     * 登录测试
     *
     * @param loginId 登录Id
     * @return token
     */
    @GetMapping("/doLogin")
    public String doLogin(String loginId) {
        SecurityUtil.doLogin(loginId, new SecurityLoginParams());
        SecuritySession securitySession = SecurityUtil.getCurrentSecuritySession();
        User user = new User();
        user.setName("zs");
        user.setTime(LocalDateTime.now());
        securitySession.setAttribute("user" + loginId, user);
        return securitySession.getCurrentSecurityToken().getToken();
    }

    /**
     * 获取session信息
     *
     * @return SecuritySession
     */
    @GetMapping("/getSessionInfo")
    public SecuritySession getSessionInfo() {
        return SecurityUtil.getCurrentSecuritySession();
    }

    /**
     * 忽略注解测试
     *
     * @return String
     */
    @GetMapping("/getIgnore")
    @SecurityIgnore
    public String getIgnore() {
        return "ok";
    }

    /**
     * 该接口需要admin角色访问
     *
     * @return String
     */
    @GetMapping("/getCheckRoleAdmin")
    @SecurityCheckRole("admin")
    public String getCheckRoleAdmin() {
        return "ok";
    }

    /**
     * 该接口需要user角色访问
     *
     * @return String
     */
    @GetMapping("/getCheckRoleUser")
    @SecurityCheckRole(value = {"user", "user3"},conditionType = SecurityConditionType.AND)
    public String getCheckRoleUser() {
        return "ok";
    }

    /**
     * 该接口需要btn:add操作权限访问
     *
     * @return String
     */
    @GetMapping("/getCheckPermission1")
    @SecurityCheckPermission("btn:add")
    public String getCheckPermission1() {
        return "ok";
    }

    /**
     * 该接口需要btn:delete操作权限访问
     *
     * @return String
     */
    @GetMapping("/getCheckPermission2")
    @SecurityCheckPermission("btn:delete")
    public String getCheckPermission2() {
        return "ok";
    }

    /**
     * 默认
     *
     * @return String
     */
    @GetMapping("/getDefault")
    public String getDefault() {
        return "ok";
    }

    /**
     * 查询列表
     *
     * @return String
     */
    @GetMapping("/getSessionInfoList")
    public SecurityPagination getSessionInfoList(Integer page, Integer limit) {
        return SecurityUtil.querySecuritySessionList(null, page, limit, false);
    }

    /**
     * 切换身份测试
     *
     * @return String
     */
    @GetMapping("/identityTempSwitching")
    public String identityTempSwitching(String loginId) {
        SecuritySession session = SecurityUtil.getCurrentSecuritySession();
        System.out.println("切换前当前登录用户" + session.getLoginId());
        SecurityUtil.identityTempSwitching(loginId, () -> {
            SecuritySession session1 = SecurityUtil.getCurrentSecuritySession();
            System.out.println("切换中当前登录用户" + session1.getLoginId());
        });
        SecuritySession session2 = SecurityUtil.getCurrentSecuritySession();
        System.out.println("切换后当前登录用户" + session2.getLoginId());
        return "ok";
    }

    @SneakyThrows
    @GetMapping("/code")
    @SecurityIgnore
    public void code(HttpServletResponse response) {
        String code = CaptchaUtil.outputVerifyImage(100, 30, response.getOutputStream(), 4);
        System.out.println(code);
    }

}
