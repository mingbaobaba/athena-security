package io.gitee.mingbaobaba.security.example.configuration;

import io.gitee.mingbaobaba.security.core.listener.SecurityListenerManager;
import io.gitee.mingbaobaba.security.example.listener.MyLoginListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * <p>description</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 16:06
 */
@Configuration
@Component
@Slf4j
public class AutoConfiguration {


    @Bean
    public void regListener() {
        log.info("注册监听");
        SecurityListenerManager.registerListener(new MyLoginListener());
    }

    @PostConstruct
    public void postConstruct() {
        log.info("配置启动");

    }

}
