package io.gitee.mingbaobaba.security.example.listener;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.listener.SecurityListener;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>description</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 16:08
 */
@Slf4j
public class MyLoginListener implements SecurityListener {


    @Override
    public void doLogin(String loginId, String token, SecurityLoginParams loginModel) {

    }

    @Override
    public void doKickOut(String loginId, String token, String deviceType) {

    }

    @Override
    public void doReplaceOut(String loginId, String token, String deviceType) {

    }

    @Override
    public void doBanned(String loginId, String token, String deviceType) {

    }

    @Override
    public void doUnseal(String loginId, String token, String deviceType) {

    }

    @Override
    public void doRenewal(String loginId, String token, String deviceType) {

    }

    @Override
    public void doRemove(String loginId, String token, String deviceType) {

    }

    @Override
    public void doLoginOut(String loginId, String token, String deviceType) {

    }

    @Override
    public void doCreatedSecuritySession(String securitySessionId) {

    }

    @Override
    public void doDestroySecuritySession(String securitySessionId) {

    }
}
