package io.gitee.mingbaobaba.security.example.service;

import io.gitee.mingbaobaba.security.core.authority.SecurityAuthority;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>权限实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/31 10:23
 */
@Service
public class MyPermissionServiceImpl implements SecurityAuthority {
    @Override
    public List<String> getPermissionCodeList(String loginId) {
        List<String> permissionList = new ArrayList<>();
        permissionList.add("btn:add");
        //permissionList.add("btn:delete");
        return permissionList;
    }

    @Override
    public List<String> getRoleCodeList(String loginId) {
        List<String> roleList = new ArrayList<>();
        roleList.add("admin");
        roleList.add("user");
        roleList.add("user1");
        roleList.add("user2");
        return roleList;
    }
}
