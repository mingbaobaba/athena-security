package io.gitee.mingbaobaba.security.example.domain;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * <p>Description</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/4 19:33
 */
@Data
public class User {

    private String name;

    private LocalDateTime time;
}
