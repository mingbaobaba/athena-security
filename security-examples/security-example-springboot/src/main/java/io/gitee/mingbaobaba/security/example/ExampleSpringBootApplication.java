package io.gitee.mingbaobaba.security.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * <p>test服务启动类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/5/31 14:41
 */
@SpringBootApplication(scanBasePackages = "io.gitee.mingbaobaba.security.example")

public class ExampleSpringBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExampleSpringBootApplication.class, args);
    }
}
