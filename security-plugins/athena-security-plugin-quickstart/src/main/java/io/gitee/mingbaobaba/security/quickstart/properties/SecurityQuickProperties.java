package io.gitee.mingbaobaba.security.quickstart.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * <p>登录参数配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 6:16
 */
@Data
@Configuration(proxyBeanMethods = false)
@ConfigurationProperties(prefix = "athena-security.quick")
public class SecurityQuickProperties {

    /**
     * ticket过期时间（单位秒） 默认60秒
     */
    private Long ticketTimeOut = 60L;

}
