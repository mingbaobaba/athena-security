package io.gitee.mingbaobaba.security.quickstart.configuration;


import io.gitee.mingbaobaba.security.quickstart.SecurityQuickManager;
import io.gitee.mingbaobaba.security.quickstart.endpoint.QuickstartEndpoint;
import io.gitee.mingbaobaba.security.quickstart.properties.SecurityQuickProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@Import({SecurityQuickProperties.class,
        QuickstartEndpoint.class})
@Slf4j
public class SecurityAutoConfiguration {

    /**
     * 配置文件
     */
    @Autowired(required = false)
    private SecurityQuickProperties securityLoginProperties;

    /**
     * 配置文件设置
     */
    @Bean
    public void setLoginConfig() {
        SecurityQuickManager.setConfig(securityLoginProperties);
    }

}
