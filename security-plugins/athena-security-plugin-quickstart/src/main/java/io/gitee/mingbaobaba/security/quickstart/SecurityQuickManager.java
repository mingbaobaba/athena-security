package io.gitee.mingbaobaba.security.quickstart;

import io.gitee.mingbaobaba.security.quickstart.properties.SecurityQuickProperties;

import java.util.Objects;

/**
 * <p>快速启动管理</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 20:58
 */
public class SecurityQuickManager {

    private SecurityQuickManager() {

    }

    private static SecurityQuickProperties config = null;

    /**
     * 获取配置
     *
     * @return SecurityLoginProperties
     */
    public static SecurityQuickProperties getConfig() {
        if (Objects.isNull(SecurityQuickManager.config)) {
            synchronized (SecurityQuickManager.class) {
                if (Objects.isNull(SecurityQuickManager.config)) {
                    SecurityQuickManager.setConfig(new SecurityQuickProperties());
                }
            }
        }
        return SecurityQuickManager.config;
    }

    /**
     * 设置配置接口
     *
     * @param config 配置
     */
    public static void setConfig(SecurityQuickProperties config) {
        SecurityQuickManager.config = config;
    }



}
