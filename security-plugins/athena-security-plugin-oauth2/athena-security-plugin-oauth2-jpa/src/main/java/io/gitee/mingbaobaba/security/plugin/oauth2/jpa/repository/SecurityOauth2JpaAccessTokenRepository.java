package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.repository;

import io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity.SecurityOauth2AccessTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>SecurityOauth2AccessTokenEntity JPA Repository </p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 5:04
 */
@Repository
public interface SecurityOauth2JpaAccessTokenRepository extends JpaRepository<SecurityOauth2AccessTokenEntity, String> {

    /**
     * 根据accessToken 查询
     * @param accessToken 访问token
     * @return SecurityOauth2AccessTokenEntity
     */
    SecurityOauth2AccessTokenEntity findByAccessToken(String accessToken);

}
