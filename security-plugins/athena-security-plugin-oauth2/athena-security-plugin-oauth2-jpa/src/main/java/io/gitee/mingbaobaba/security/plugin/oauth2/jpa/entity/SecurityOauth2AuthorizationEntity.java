package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * <p>SecuritySession实体类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:38
 */
@Data
@Entity
@Table(name = "security_oauth2_authorization")
public class SecurityOauth2AuthorizationEntity {

    /**
     * 登录Id
     */
    @Id
    private String authorizationCode;

    /**
     * authorization data
     */
    @Lob
    private String authorizationData;

    /**
     * 超时时间(秒)
     */
    private Long timeout;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
