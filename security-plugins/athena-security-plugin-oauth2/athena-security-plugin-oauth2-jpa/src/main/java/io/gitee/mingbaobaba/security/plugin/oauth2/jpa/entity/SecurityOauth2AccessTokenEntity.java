package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * <p>security_oauth2_access_token</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:50
 */
@Data
@Entity
@Table(name = "security_oauth2_access_token")
public class SecurityOauth2AccessTokenEntity {
    /**
     * 刷新token
     */
    @Id
    private String refreshToken;

    /**
     * 访问token
     */
    private String accessToken;

    /**
     * 登录Id
     */
    private String loginId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 客户端Id
     */
    private String clientId;

    /**
     * 超时时间(秒)
     */
    private Long timeout;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
