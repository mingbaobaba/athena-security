package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.repository;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Application;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2ApplicationRepository;
import io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity.SecurityOauth2ApplicationEntity;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

/**
 * <p>客户端查询实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 13:34
 */
public class SecurityOauth2JpaApplicationClientRepository implements SecurityOauth2ApplicationRepository {

    @Autowired
    private SecurityOauth2JpaApplicationRepository securityOauth2JpaApplicationRepository;

    @Override
    public SecurityOauth2Application getOauth2ApplicationByClientId(String clientId) {
        Optional<SecurityOauth2ApplicationEntity> optional = securityOauth2JpaApplicationRepository.findById(clientId);
        if(!optional.isPresent()){
            return null;
        }
        SecurityOauth2ApplicationEntity entity = optional.get();
        return SecurityOauth2Application.builder()
                .clientId(entity.getClientId())
                .clientName(entity.getClientName())
                .clientSecret(entity.getClientSecret())
                .grantType(entity.getGrantType())
                .scope(entity.getScope())
                .redirectUri(entity.getRedirectUri())
                .build();
    }
}
