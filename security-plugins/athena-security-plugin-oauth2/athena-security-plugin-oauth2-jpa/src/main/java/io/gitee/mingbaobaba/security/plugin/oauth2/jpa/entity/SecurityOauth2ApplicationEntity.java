package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * <p>SecurityApplicationEntity实体类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:38
 */
@Data
@Entity
@Table(name = "security_oauth2_application")
public class SecurityOauth2ApplicationEntity {

    /**
     * 客户端ID
     */
    @Id
    private String clientId;

    /**
     * 客户端名称
     */
    private String clientName;

    /**
     * 客户端密钥
     */
    private String clientSecret;

    /**
     * 授权范围,多个以逗号分割
     */
    private String scope;

    /**
     * 回调地址,多个以逗号分割
     */
    @Column(length = 1024)
    private String redirectUri;

    /**
     * 授权类型,多个以逗号分割
     */
    private String grantType;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

}
