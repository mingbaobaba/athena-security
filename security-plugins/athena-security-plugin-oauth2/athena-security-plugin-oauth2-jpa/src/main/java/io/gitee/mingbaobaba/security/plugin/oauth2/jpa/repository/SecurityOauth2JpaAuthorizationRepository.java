package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.repository;

import io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity.SecurityOauth2AuthorizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * <p>SecurityOauth2AccessTokenEntity JPA Repository </p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 5:04
 */
@Repository
public interface SecurityOauth2JpaAuthorizationRepository extends JpaRepository<SecurityOauth2AuthorizationEntity, String> {


}
