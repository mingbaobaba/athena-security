package io.gitee.mingbaobaba.security.plugin.oauth2.jpa.repository;


import io.gitee.mingbaobaba.security.plugin.oauth2.jpa.entity.SecurityOauth2ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>SecurityOauth2ApplicationEntity JPA Repository</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 5:02
 */
@Repository
public interface SecurityOauth2JpaApplicationRepository extends JpaRepository<SecurityOauth2ApplicationEntity,String> {

}
