package io.gitee.mingbaobaba.security.plugin.oauth2.redis.constants;

/**
 * <p>oauth2认证相关常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/29 10:17
 */
public interface SecurityOauth2RedisConstant {

    /**
     * key前缀
     */
    String KEY_PREFIX = "athena:security:oauth2";

    /**
     * AuthorizationCode信息key
     * {0} authorizationCode
     */
    String AUTHORIZATION_CODE_KEY = KEY_PREFIX + ":code:{0}";

    /**
     * 访问token信息key
     * {0} accessToken
     */
    String ACCESS_TOKEN_KEY = KEY_PREFIX + ":token:access:{0}";

    /**
     * 刷新token ken
     * {0} refreshToken
     */
    String REFRESH_TOKEN_KEY = KEY_PREFIX + ":token:refresh:{0}";
}
