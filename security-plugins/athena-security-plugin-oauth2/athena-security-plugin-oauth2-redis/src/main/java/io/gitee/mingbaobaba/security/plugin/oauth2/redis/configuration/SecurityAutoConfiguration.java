package io.gitee.mingbaobaba.security.plugin.oauth2.redis.configuration;

import io.gitee.mingbaobaba.security.oauth2.SecurityOauth2Manager;
import io.gitee.mingbaobaba.security.plugin.oauth2.redis.repository.SecurityOauth2RedisRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@Slf4j
public class SecurityAutoConfiguration {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * repository对象注入
     */
    @Bean
    public void securityOauth2RedisRepository() {
        SecurityOauth2Manager.setSecurityOauth2Repository(new SecurityOauth2RedisRepository(stringRedisTemplate));
    }
}
