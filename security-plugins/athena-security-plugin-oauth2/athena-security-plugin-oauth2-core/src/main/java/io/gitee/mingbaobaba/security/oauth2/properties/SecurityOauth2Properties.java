package io.gitee.mingbaobaba.security.oauth2.properties;

import lombok.Data;

/**
 * <p>Oauth2配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 10:21
 */
@Data
public class SecurityOauth2Properties {

    /**
     * accessToken超时时间 默认2小时
     */
    private Long accessTokenTimeout = 2L * 60 * 60;

    /**
     * refreshToken超时时间 默认24小时
     */
    private Long refreshTokenTimeout = 24L * 60 * 60;

    /**
     * 是否自动同意确认授权
     */
    private Boolean autoAgreeAuthorization = false;

    /**
     * 登录页面
     */
    private String loginPage;

    /**
     * 确认页面
     */
    private String confirmPage;

    /**
     * 撤销页面
     */
    private String revokePage;

}
