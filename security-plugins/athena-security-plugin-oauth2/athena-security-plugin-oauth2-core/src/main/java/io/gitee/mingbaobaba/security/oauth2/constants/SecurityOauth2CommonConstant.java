package io.gitee.mingbaobaba.security.oauth2.constants;

/**
 * <p>Oauth2公共常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 5:01
 */
public interface SecurityOauth2CommonConstant {

    /**
     * 返回类型code
     */
    String RESPONSE_TYPE_CODE = "code";

    /**
     * 返回类型token
     */
    String RESPONSE_TYPE_TOKEN = "token";

    /**
     * 同意
     */
    String CONFIRM_TYPE_APPROVE = "approve";

    /**
     * 拒绝
     */
    String CONFIRM_TYPE_DENY = "deny";

    /**
     * Basic header名称
     */
    String HEADER_NAME_BASIC = "Basic";

    /**
     * 空字符串
     */
    String EMPTY_STR = "";

}
