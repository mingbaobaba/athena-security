package io.gitee.mingbaobaba.security.oauth2.domain;



import java.io.Serializable;


/**
 * <p>token接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 20:15
 */
public interface SecurityOAuth2Token extends Serializable {

    default String getIssuedAt() {
        return null;
    }


    default Long getExpiresIn() {
        return null;
    }
}
