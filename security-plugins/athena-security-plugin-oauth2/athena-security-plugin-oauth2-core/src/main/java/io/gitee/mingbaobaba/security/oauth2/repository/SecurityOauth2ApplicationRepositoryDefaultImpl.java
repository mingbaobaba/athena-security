package io.gitee.mingbaobaba.security.oauth2.repository;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Application;

/**
 * <p>应用信息查询接口默认实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 16:14
 */

public class SecurityOauth2ApplicationRepositoryDefaultImpl implements SecurityOauth2ApplicationRepository {

    @Override
    public SecurityOauth2Application getOauth2ApplicationByClientId(String clientId) {
        return SecurityOauth2Application.builder().build();
    }
}
