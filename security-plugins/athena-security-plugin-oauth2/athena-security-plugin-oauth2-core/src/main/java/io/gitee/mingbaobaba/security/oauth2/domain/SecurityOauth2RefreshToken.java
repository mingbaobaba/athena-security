package io.gitee.mingbaobaba.security.oauth2.domain;


import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>刷新token</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 20:26
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SecurityOauth2RefreshToken extends SecurityOauth2AccessToken {

}
