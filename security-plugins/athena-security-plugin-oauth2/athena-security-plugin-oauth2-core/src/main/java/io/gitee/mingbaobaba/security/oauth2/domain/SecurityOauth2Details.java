package io.gitee.mingbaobaba.security.oauth2.domain;

import lombok.*;

import java.io.Serializable;

/**
 * <p>oauth2授权详情</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2024/3/13 22:56
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecurityOauth2Details extends SecurityOauth2AccessToken implements Serializable {

    /**
     * 客户端Id
     */
    private String clientId;

    /**
     * 登录Id
     */
    private String loginId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 姓名
     */
    private String name;

}
