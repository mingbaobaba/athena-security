package io.gitee.mingbaobaba.security.oauth2.constants;

/**
 * <p>异常码</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 4:51
 */
public interface SecurityOauth2ErrorCodeConstant {

    /**
     * 客户端未授权
     */
    String OAUTH2_CODE_CLIENT_NO_AUTH = "2001";

    /**
     * 客户端密钥错误
     */
    String OAUTH2_CODE_CLIENT_SECRET_ERR = "2002";

    /**
     * 客户端密钥错误
     */
    String OAUTH2_CODE_CLIENT_SCOPE_ERR = "2003";

    /**
     * 客户端回调地址错误
     */
    String OAUTH2_CODE_CLIENT_REDIRECT_URI_ERR = "2004";

    /**
     * 客户端返回类型错误
     */
    String OAUTH2_CODE_CLIENT_RESPONSE_TYPE_ERR = "2005";

    /**
     * 无效的授权码
     */
    String OAUTH2_CODE_INVALID_AUTHORIZATION_CODE = "2006";

    /**
     * 保存accessToken异常
     */
    String OAUTH2_CODE_SAVE_ACCESS_CODE_ERR = "2007";

    /**
     * 未识别的responseType
     */
    String OAUTH2_CODE_UNRECOGNIZED_RESPONSE_TYPE = "2008";

    /**
     * 无效的刷新token
     */
    String OAUTH2_CODE_INVALID_REFRESH_TOKEN = "2009";

    /**
     * 无效的访问token
     */
    String OAUTH2_CODE_INVALID_ACCESS_TOKEN = "2010";

    /**
     * 未识别的grantType
     */
    String OAUTH2_CODE_UNRECOGNIZED_GRANT_TYPE = "2011";


}
