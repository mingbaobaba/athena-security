package io.gitee.mingbaobaba.security.oauth2.enums;


/**
 * <p>授权类型</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 10:21
 */
public enum GrantType {


    /**
     * 授权码模式
     */
    AUTHORIZATION_CODE("authorization_code", "授权码模式"),
    /**
     * 隐式模式
     */
    IMPLICIT("implicit", "隐式模式"),
    /**
     * 密码模式
     */
    PASSWORD("password", "密码模式"),
    /**
     * 客户端模式
     */
    CLIENT_CREDENTIALS("client_credentials", "客户端模式");


    private String code;
    private String name;

    private GrantType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
