package io.gitee.mingbaobaba.security.oauth2.repository;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Client;
import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Details;


/**
 * <p>oauth2数据持久化</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 4:40
 */
public interface SecurityOauth2Repository {
    /**
     * 保存授权码相关信息
     *
     * @param authorizationCode  授权码
     * @param securityClientMode 授权客户端信息
     * @param timeout            超时时间(秒)
     */
    void saveAuthorizationCode(String authorizationCode, SecurityOauth2Client securityClientMode, long timeout);

    /**
     * 根据授权码获取授权客户端相关信息
     *
     * @param authorizationCode 授权码
     * @return SecurityClientModel
     */
    SecurityOauth2Client getClientModelByAuthorizationCode(String authorizationCode);

    /**
     * 删除授权码信息
     *
     * @param authorizationCode 授权码
     */
    void removeAuthorizationCode(String authorizationCode);

    /**
     * 保存accessToken详情
     *
     * @param securityOauth2Details 详情
     * @param timeout               有效期
     * @return 是否保存成功
     */
    boolean saveOauth2TokenDetails(SecurityOauth2Details securityOauth2Details, Long timeout);

    /**
     * 根据accessToken获取详情
     *
     * @param accessToken 访问token
     * @return SecurityOauth2Details
     */
    SecurityOauth2Details getOauth2DetailsByAccessToken(String accessToken);

    /**
     * 根据刷新token获取访问Token
     *
     * @param refreshToken 刷新token
     * @return 访问token
     */
    String accessTokenByRefreshToken(String refreshToken);

    /**
     * 获取刷新token超时时间
     *
     * @param refreshToken 刷新token
     * @return 超时时间
     */
    Long refreshTokenTimeOut(String refreshToken);

}
