package io.gitee.mingbaobaba.security.oauth2.repository;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Application;

/**
 * <p>授权应用信息</p>
 * <p>
 * 此接口需要开发者自己实现
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 16:12
 */

public interface SecurityOauth2ApplicationRepository {

    /**
     * 查询应用信息
     *
     * @param clientId 客户端Id
     * @return SecurityOauth2Application
     */
    SecurityOauth2Application getOauth2ApplicationByClientId(String clientId);

}
