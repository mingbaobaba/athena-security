package io.gitee.mingbaobaba.security.oauth2;

import io.gitee.mingbaobaba.security.oauth2.properties.SecurityOauth2Properties;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2Repository;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2RepositoryDefaultImpl;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2ApplicationRepository;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2ApplicationRepositoryDefaultImpl;
import io.gitee.mingbaobaba.security.oauth2.service.SecurityOauth2Service;
import io.gitee.mingbaobaba.security.oauth2.service.SecurityOauth2ServiceImpl;

import java.util.Objects;

/**
 * <p>oauth2管理</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/8 4:37
 */
public class SecurityOauth2Manager {
    private SecurityOauth2Manager() {

    }

    private static SecurityOauth2Properties config = null;

    /**
     * 获取配置
     *
     * @return SecurityLoginProperties
     */
    public static SecurityOauth2Properties getConfig() {
        if (Objects.isNull(SecurityOauth2Manager.config)) {
            synchronized (SecurityOauth2Manager.class) {
                if (Objects.isNull(SecurityOauth2Manager.config)) {
                    SecurityOauth2Manager.setConfig(new SecurityOauth2Properties());
                }
            }
        }
        return SecurityOauth2Manager.config;
    }

    /**
     * 设置配置接口
     *
     * @param config 配置
     */
    public static void setConfig(SecurityOauth2Properties config) {
        SecurityOauth2Manager.config = config;
    }

    /**
     * Repository接口
     */
    private static SecurityOauth2Repository securityOauth2Repository = null;

    /**
     * 获取Repository接口
     *
     * @return SecurityOauth2Service
     */
    public static SecurityOauth2Repository getSecurityOauth2Repository() {
        if (Objects.isNull(SecurityOauth2Manager.securityOauth2Repository)) {
            synchronized (SecurityOauth2Manager.class) {
                if (Objects.isNull(SecurityOauth2Manager.securityOauth2Repository)) {
                    SecurityOauth2Manager.setSecurityOauth2Repository(new SecurityOauth2RepositoryDefaultImpl());
                }
            }
        }
        return SecurityOauth2Manager.securityOauth2Repository;
    }

    /**
     * 设置Repository接口
     *
     * @param securityOauth2Repository SecurityOauth2Repository
     */
    public static void setSecurityOauth2Repository(SecurityOauth2Repository securityOauth2Repository) {
        SecurityOauth2Manager.securityOauth2Repository = securityOauth2Repository;
    }


    /**
     * service接口
     */
    private static SecurityOauth2Service securityOauth2Service = null;

    /**
     * 获取service接口
     *
     * @return SecurityOauth2Service
     */
    public static SecurityOauth2Service getSecurityOauth2Service() {
        if (Objects.isNull(SecurityOauth2Manager.securityOauth2Service)) {
            synchronized (SecurityOauth2Manager.class) {
                if (Objects.isNull(SecurityOauth2Manager.securityOauth2Service)) {
                    SecurityOauth2Manager.setSecurityOauth2Service(new SecurityOauth2ServiceImpl());
                }
            }
        }
        return SecurityOauth2Manager.securityOauth2Service;
    }

    /**
     * 设置service接口
     *
     * @param securityOauth2Service SecurityOauth2Service
     */
    public static void setSecurityOauth2Service(SecurityOauth2Service securityOauth2Service) {
        SecurityOauth2Manager.securityOauth2Service = securityOauth2Service;
    }


    /**
     * SecurityOauth2ClientRepository接口
     */
    private static SecurityOauth2ApplicationRepository securityOauth2ApplicationRepository = null;

    /**
     * 获取SecurityOauth2ApplicationRepository接口
     *
     * @return SecurityOauth2ApplicationRepository
     */
    public static SecurityOauth2ApplicationRepository getSecurityOauth2ApplicationRepository() {
        if (Objects.isNull(SecurityOauth2Manager.securityOauth2ApplicationRepository)) {
            synchronized (SecurityOauth2Manager.class) {
                if (Objects.isNull(SecurityOauth2Manager.securityOauth2ApplicationRepository)) {
                    SecurityOauth2Manager.setSecurityOauth2ApplicationRepository(new SecurityOauth2ApplicationRepositoryDefaultImpl());
                }
            }
        }
        return SecurityOauth2Manager.securityOauth2ApplicationRepository;
    }

    /**
     * 设置SecurityOauth2ApplicationRepository接口
     *
     * @param securityOauth2ApplicationRepository SecurityOauth2ApplicationRepository
     */
    public static void setSecurityOauth2ApplicationRepository(SecurityOauth2ApplicationRepository securityOauth2ApplicationRepository) {
        SecurityOauth2Manager.securityOauth2ApplicationRepository = securityOauth2ApplicationRepository;
    }

}
