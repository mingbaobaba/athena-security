package io.gitee.mingbaobaba.security.oauth2.domain;


import lombok.Data;

import java.io.Serializable;

/**
 * <p>description</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 20:20
 */
@Data
public abstract class AbstractSecurityOAuth2Token implements SecurityOAuth2Token, Serializable {

    /**
     * 签发时间
     */
    private String issuedAt;

    /**
     * 过期时间
     */
    private Long expiresIn;


}
