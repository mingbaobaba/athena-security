package io.gitee.mingbaobaba.security.oauth2.domain;


import lombok.*;

import java.io.Serializable;


/**
 * <p>登录参数构建</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 4:29
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SecurityOauth2Client implements Serializable {

    /**
     * 客户端ID
     */
    private String clientId;

    /**
     * 客户端名称
     */
    private String clientName;


    /**
     * 客户端密钥
     */
    private String clientSecret;

    /**
     * 返回类型
     */
    private String responseType;

    /**
     * 授权范围
     */
    private String scope;

    /**
     * 回调地址
     */
    private String redirectUri;

    /**
     * 状态
     */
    private String state;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 用户token值
     */
    private String token;


}
