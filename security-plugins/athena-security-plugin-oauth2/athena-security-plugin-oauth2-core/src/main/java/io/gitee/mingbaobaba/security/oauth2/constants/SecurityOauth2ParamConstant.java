package io.gitee.mingbaobaba.security.oauth2.constants;

/**
 * <p>参数常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 22:15
 */
public interface SecurityOauth2ParamConstant {
    String RESPONSE_TYPE = "response_type";
    String CLIENT_ID = "client_id";
    String CLIENT_NAME = "client_name";
    String CLIENT_SECRET = "client_secret";
    String REDIRECT_URI = "redirect_uri";
    String SCOPE = "scope";
    String STATE = "state";
    String CODE = "code";
    String GRANT_TYPE = "grant_type";
    String LOGIN_ID = "loginId";
    String USERNAME = "username";
    String NAME = "name";
    String PASSWORD = "password";
    String CONFIRM_TYPE = "confirm_type";
    String REFRESH_TOKEN = "refresh_token";
    String ACCESS_TOKEN = "access_token";
    String LOGIN_TITLE = "loginTitle";
    String COPYRIGHT = "copyright";
    String ERROR_MSG = "errorMsg";

}
