package io.gitee.mingbaobaba.security.oauth2.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>访问Token</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 13:56
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class SecurityOauth2AccessToken extends AbstractSecurityOAuth2Token {

    private String accessToken;

    private String refreshToken;

    private Long refreshExpiresIn;

    private String tokenType = "Bearer";


}
