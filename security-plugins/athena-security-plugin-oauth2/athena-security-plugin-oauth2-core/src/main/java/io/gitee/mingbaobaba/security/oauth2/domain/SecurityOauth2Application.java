package io.gitee.mingbaobaba.security.oauth2.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>oauth2 授权应用信息</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/12 18:04
 */
@Getter
@Setter
@Builder
public class SecurityOauth2Application {

    /**
     * 客户端ID
     */
    private String clientId;

    /**
     * 客户端名称
     */
    private String clientName;

    /**
     * 客户端密钥
     */
    private String clientSecret;

    /**
     * 授权范围,多个以逗号分割
     */
    private String scope;

    /**
     * 回调地址,多个以逗号分割
     */
    private String redirectUri;

    /**
     * 授权类型,多个以逗号分割
     */
    private String grantType;

}
