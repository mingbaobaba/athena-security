package io.gitee.mingbaobaba.security.oauth2.exception;

import io.gitee.mingbaobaba.security.core.exception.SecurityBaseException;

/**
 * <p>oauth2异常类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 10:32
 */
public class SecurityOauth2Exception extends SecurityBaseException {

    public SecurityOauth2Exception(String message) {
        super(message);
    }

    public SecurityOauth2Exception(String code, String message) {
        super(code, message);
    }
}
