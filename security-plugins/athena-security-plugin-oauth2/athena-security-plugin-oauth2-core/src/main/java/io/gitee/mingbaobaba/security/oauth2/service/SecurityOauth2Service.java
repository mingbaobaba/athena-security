package io.gitee.mingbaobaba.security.oauth2.service;

import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2AccessToken;
import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Details;
import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2RefreshToken;
import io.gitee.mingbaobaba.security.oauth2.enums.GrantType;
import io.gitee.mingbaobaba.security.oauth2.domain.SecurityOauth2Client;

/**
 * <p>授权服务</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 21:54
 */
public interface SecurityOauth2Service {

    /**
     * 构建登录参数
     *
     * @param grantType 授权类型
     * @return SecurityLoginModel
     */
    SecurityOauth2Client buildLoginModel(GrantType grantType);

    /**
     * 生成授权码
     *
     * @return 授权码
     */
    String generateAuthorizationCode();

    /**
     * 生成授权码
     * @param username 用户名
     * @return 授权码
     */
    String generateAuthorizationCode(String tokenValue);

    /**
     * 构建授权码返回地址
     *
     * @return 授权码地址
     */
    String buildAuthorizationCodeUri(String authorizationCode);

    /**
     * 根据授权码换取SecurityOauth2AccessToken信息
     *
     * @param authorizationCode 授权码
     * @return SecurityOauth2AccessToken
     */
    SecurityOauth2AccessToken getAccessTokenByAuthorizationCode(String authorizationCode);

    /**
     * 撤销授权
     *
     * @param authorizationCode 授权码
     */
    void revokeAuthorization(String authorizationCode);

    /**
     * 授权登录
     *
     * @param clientModel SecurityClientModel
     * @param grantType   授权类型
     * @return SecurityOauth2AccessToken
     */
    SecurityOauth2AccessToken grantAuthorizationLogin(SecurityOauth2Client clientModel, GrantType grantType);

    /**
     * 构建隐式授权返回
     *
     * @return 地址
     */
    String buildImplicitGrantUri();

    /**
     * 刷新token
     *
     * @return SecurityOauth2RefreshToken
     */
    SecurityOauth2RefreshToken refreshToken();

    /**
     * 获取用户信息
     *
     * @param accessToken 登录token
     * @return SecurityOauth2Details
     */
    SecurityOauth2Details getUserInfo(String accessToken);

}
