package io.gitee.mingbaobaba.security.oauth2.constants;

/**
 * <p>Oauth2 api常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 21:56
 */
public interface SecurityOauth2ApiConstant {

    String AUTHORIZE_URL = "/oauth2/authorize";
    String LOGIN_URL = "/oauth2/login";
    String CONFIRM_URL = "/oauth2/confirm";
    String REVOKE_URL = "/oauth2/revoke";
    String USERINFO_URL = "/oauth2/userinfo";
    String TOKEN_URL = "/oauth2/token";
    String REFRESH_URL = "/oauth2/refresh";

}
