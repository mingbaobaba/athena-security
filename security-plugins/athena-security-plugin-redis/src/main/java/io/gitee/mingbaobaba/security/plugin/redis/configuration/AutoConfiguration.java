package io.gitee.mingbaobaba.security.plugin.redis.configuration;


import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;
import io.gitee.mingbaobaba.security.plugin.redis.repository.SecurityCaptchaRedisRepository;
import io.gitee.mingbaobaba.security.plugin.redis.repository.SecurityRedisRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;



/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@Slf4j
public class AutoConfiguration {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    /**
     * repository对象注入
     */
    @Bean
    public void securityRedisRepository() {
        SecurityFactory.setSecurityRepository.accept(new SecurityRedisRepository(stringRedisTemplate));
    }

    /**
     * repository captcha 对象注入
     */
    @Bean
    public void securityCaptchaRedisRepository() {
        SecurityFactory.setSecurityCaptchaRepository.accept(new SecurityCaptchaRedisRepository(stringRedisTemplate));
    }


}
