package io.gitee.mingbaobaba.security.plugin.redis.constants;

/**
 * <p>认证相关常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/29 10:17
 */
public interface SecurityRedisConstant {

    /**
     * key前缀
     */
    String KEY_PREFIX = "athena:security";

    /**
     * session信息key
     * {0} loginId
     */
    String SESSION_INFO_KEY = KEY_PREFIX + ":session:{0}";

    /**
     * 登录ID关联session信息key
     * {0} token
     */
    String TOKEN_REL_LOGIN_ID_KEY = KEY_PREFIX + ":token:{0}";

    /**
     * 验证码 key
     */
    String CAPTCHA_KEY = KEY_PREFIX + ":captcha:{0}:{1}";

}
