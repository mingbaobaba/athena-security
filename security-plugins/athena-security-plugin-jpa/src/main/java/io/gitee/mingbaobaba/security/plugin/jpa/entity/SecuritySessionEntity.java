package io.gitee.mingbaobaba.security.plugin.jpa.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * <p>SecuritySession实体类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:38
 */
@Data
@Entity
@Table(name = "security_session")
public class SecuritySessionEntity {

    /**
     * 登录Id
     */
    @Id
    private String loginId;

    /**
     * session data
     */
    @Lob
    private String sessionData;

    /**
     * 超时时间(秒)
     */
    private Long timeout;

    /**
     * 版本
     */
    @Version
    private Long version;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}
