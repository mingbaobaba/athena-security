package io.gitee.mingbaobaba.security.plugin.jpa.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * <p>验证码</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/21 17:25
 */
@Data
@Entity
@Table(name = "security_captcha")
public class SecurityCaptchaEntity {

    @Id
    private String code;

    /**
     * 过期时间
     */
    private LocalDateTime expiryTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

}
