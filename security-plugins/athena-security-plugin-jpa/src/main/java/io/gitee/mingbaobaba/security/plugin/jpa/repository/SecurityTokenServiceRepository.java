package io.gitee.mingbaobaba.security.plugin.jpa.repository;

import io.gitee.mingbaobaba.security.plugin.jpa.entity.SecurityTokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>SecurityToken JPA Repository </p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 5:04
 */
@Repository
public interface SecurityTokenServiceRepository extends JpaRepository<SecurityTokenEntity, String>, JpaSpecificationExecutor<SecurityTokenEntity> {

    List<SecurityTokenEntity> findByTokenLike(String token);

}
