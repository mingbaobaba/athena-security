package io.gitee.mingbaobaba.security.plugin.jpa.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * <p>security_token实体类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:50
 */
@Data
@Entity
@Table(name = "security_token")
public class SecurityTokenEntity {
    /**
     * token值
     */
    @Id
    private String token;

    /**
     * 登录Id
     */
    private String loginId;

    /**
     * token data
     */
    @Lob
    private String tokenData;

    /**
     * 超时时间(秒)
     */
    private Long timeout;

    /**
     * 活跃超时时间(秒)
     */
    private Long activityTimeout;

    /**
     * 活跃时间
     */
    private LocalDateTime activityTime;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;
}
