package io.gitee.mingbaobaba.security.plugin.jpa.repository;

import io.gitee.mingbaobaba.security.plugin.jpa.entity.SecurityCaptchaEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>SecurityCaptcha JPA Repository</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/21 17:46
 */
@Repository
public interface SecurityCaptchaServiceRepository extends JpaRepository<SecurityCaptchaEntity,String> {
}
