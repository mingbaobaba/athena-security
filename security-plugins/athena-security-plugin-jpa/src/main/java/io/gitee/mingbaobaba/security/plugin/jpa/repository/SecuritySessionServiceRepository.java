package io.gitee.mingbaobaba.security.plugin.jpa.repository;


import io.gitee.mingbaobaba.security.plugin.jpa.entity.SecuritySessionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <p>SecuritySession JPA Repository</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 5:02
 */
@Repository
public interface SecuritySessionServiceRepository extends JpaRepository<SecuritySessionEntity,String> {

}
