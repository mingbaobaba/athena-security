package io.gitee.mingbaobaba.security.plugin.jpa.configuration;

import io.gitee.mingbaobaba.security.plugin.jpa.repository.SecurityCaptchaJpaRepository;
import io.gitee.mingbaobaba.security.plugin.jpa.repository.SecurityJpaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;

/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@EntityScan("io.gitee.mingbaobaba.security.plugin.jpa.entity")
@EnableJpaRepositories(basePackages = "io.gitee.mingbaobaba.security.plugin.jpa.repository")
@Import({
        SecurityJpaRepository.class,
        SecurityCaptchaJpaRepository.class
})
@Slf4j
public class AutoConfiguration {

    @PostConstruct
    public void postConstruct() {
        log.info("Athena-Security [athena-security-plugin-jpa] Configure.");
    }

}
