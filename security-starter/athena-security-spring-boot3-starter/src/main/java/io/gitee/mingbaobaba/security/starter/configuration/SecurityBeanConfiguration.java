package io.gitee.mingbaobaba.security.starter.configuration;

import io.gitee.mingbaobaba.security.core.context.SecurityContext;
import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;
import io.gitee.mingbaobaba.security.core.authority.SecurityAuthority;
import io.gitee.mingbaobaba.security.core.listener.SecurityListener;
import io.gitee.mingbaobaba.security.core.listener.SecurityListenerManager;
import io.gitee.mingbaobaba.security.core.properties.SecurityProperties;
import io.gitee.mingbaobaba.security.core.repository.SecurityRepository;
import io.gitee.mingbaobaba.security.core.request.SecurityRequest;
import io.gitee.mingbaobaba.security.core.response.SecurityResponse;
import io.gitee.mingbaobaba.security.core.response.SecurityResponseWrapper;
import io.gitee.mingbaobaba.security.core.service.SecurityUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * <p>相关组件bean注入</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 9:17
 */
public class SecurityBeanConfiguration {

    /**
     * 组件注入
     * 配置文件先初始化
     *
     * @param securityProperties 配置
     */
    public SecurityBeanConfiguration(@Autowired(required = false) SecurityProperties securityProperties) {
        if (Objects.nonNull(securityProperties)) {
            SecurityFactory.setConfig.accept(securityProperties);
        }
    }

    /**
     * Request对象注入
     *
     * @param securityRequest 请求对象
     */
    @Autowired(required = false)
    public void setSecurityRequest(SecurityRequest securityRequest) {
        if (Objects.nonNull(securityRequest)) {
            SecurityFactory.setSecurityRequest.accept(securityRequest);
        }
    }

    /**
     * response对象注入
     *
     * @param securityResponse 响应对象
     */
    @Autowired(required = false)
    public void setSecurityResponse(SecurityResponse securityResponse) {
        if (Objects.nonNull(securityResponse)) {
            SecurityFactory.setSecurityResponse.accept(securityResponse);
        }
    }

    /**
     * context对象注入
     *
     * @param securityContext 上下文对象
     */
    @Autowired(required = false)
    public void setSecurityContext(SecurityContext securityContext) {
        if (Objects.nonNull(securityContext)) {
            SecurityFactory.setSecurityContext.accept(securityContext);
        }
    }

    /**
     * 权限对象注入
     *
     * @param securityAuthority 权限接口
     */
    @Autowired(required = false)
    public void setSecurityPermission(SecurityAuthority securityAuthority) {
        if (Objects.nonNull(securityAuthority)) {
            SecurityFactory.setSecurityPermission.accept(securityAuthority);
        }
    }

    /**
     * 返回包装对象注入
     *
     * @param securityResponseWrapper 返回对象包装
     */
    @Autowired(required = false)
    public void setSecurityResponseWrapper(SecurityResponseWrapper securityResponseWrapper) {
        if (Objects.nonNull(securityResponseWrapper)) {
            SecurityFactory.setSecurityResponseWrapper.accept(securityResponseWrapper);
        }
    }

    /**
     * 持久化对象注入
     *
     * @param securityRepository 持久化对象
     */
    @Autowired(required = false)
    public void setSecurityRepository(SecurityRepository securityRepository) {
        if (Objects.nonNull(securityRepository)) {
            SecurityFactory.setSecurityRepository.accept(securityRepository);
        }
    }

    /**
     * 用户明细查询注入
     *
     * @param securityUserDetailsService 用户明细查询接口服务
     */
    @Autowired(required = false)
    public void setSecurityUserDetailsService(SecurityUserDetailsService securityUserDetailsService) {
        if (Objects.nonNull(securityUserDetailsService)) {
            SecurityFactory.setSecurityUserDetailsService.accept(securityUserDetailsService);
        }
    }

    /**
     * 注册监听器
     *
     * @param securityListener SecurityListener
     */
    @Autowired(required = false)
    public void setSecurityListener(SecurityListener securityListener) {
        if (Objects.nonNull(securityListener)) {
            SecurityListenerManager.registerListener(securityListener);
        }
    }

}
