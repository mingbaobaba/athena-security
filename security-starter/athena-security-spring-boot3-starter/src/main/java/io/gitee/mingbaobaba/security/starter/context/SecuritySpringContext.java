package io.gitee.mingbaobaba.security.starter.context;

import io.gitee.mingbaobaba.security.core.context.SecurityContext;
import io.gitee.mingbaobaba.security.core.context.SecurityContextDefaultImpl;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

/**
 * <p>上下文对象</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 13:36
 */
public class SecuritySpringContext extends SecurityContextDefaultImpl implements SecurityContext {

    /**
     * 重写路由匹配算法
     *
     * @param pattern 匹配值
     * @param path    路径
     * @return boolean
     */
    @Override
    public boolean routePathMatch(String pattern, String path) {
        PathMatcher pathMatcher = new AntPathMatcher();
        return pathMatcher.match(pattern, path);
    }
}
