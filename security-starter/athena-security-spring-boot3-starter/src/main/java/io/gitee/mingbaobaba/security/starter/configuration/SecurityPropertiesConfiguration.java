package io.gitee.mingbaobaba.security.starter.configuration;

import io.gitee.mingbaobaba.security.core.properties.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * <p>配置文件bean注入</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 9:14
 */
public class SecurityPropertiesConfiguration {

    /**
     * 配置文件Bean
     *
     * @return {@link SecurityProperties}
     */
    @Bean
    @ConfigurationProperties(prefix = "athena-security")
    public SecurityProperties getSecurityProperties() {
        return new SecurityProperties();
    }

}
