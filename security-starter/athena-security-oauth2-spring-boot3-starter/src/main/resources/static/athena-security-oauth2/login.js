$(document).ready(function(){

init();

//初始化
function init(){
    $('input.ui-input').focus(function(){
        //移除异常提示信息
        $('.ui-error').html('');
    });
    //图片点击
    $("#captchaImg").click(function(){
        getCode();
    });
    getCode();
}

//获取验证码
function getCode(){
    let codeElement = $(".ui-from-item-code");
    codeElement.hide();
    $.get(ctxPath+"securityCaptcha", function (res) {
        var data = res.data||res;
        //验证是否启用验证码
        if(data.captchaEnabled){
             codeElement.show();
             $("#captchaImg").attr("src","data:image/gif;base64," + data.img);
             $('input[name="captchaSeqId"]').val(data.captchaSeqId);
        }else{
             codeElement.hide();
        }
    })
}
});

