package io.gitee.mingbaobaba.security.oauth2.starter.configuration;

import io.gitee.mingbaobaba.security.core.properties.SecurityProperties;
import io.gitee.mingbaobaba.security.oauth2.properties.SecurityOauth2Properties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

/**
 * <p>配置文件</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/10 4:17
 */
public class SecurityOauth2PropertiesConfiguration {

    /**
     * 配置文件Bean
     *
     * @return {@link SecurityProperties}
     */
    @Bean
    @ConfigurationProperties(prefix = "athena-security.oauth2")
    public SecurityOauth2Properties getSecurityOauth2Properties() {
        return new SecurityOauth2Properties();
    }

}
