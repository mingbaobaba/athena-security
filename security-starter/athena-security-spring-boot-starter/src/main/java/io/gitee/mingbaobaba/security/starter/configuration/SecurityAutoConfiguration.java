package io.gitee.mingbaobaba.security.starter.configuration;

import io.gitee.mingbaobaba.security.starter.context.SecuritySpringContext;
import io.gitee.mingbaobaba.security.starter.endpoint.CaptchaEndpoint;
import io.gitee.mingbaobaba.security.starter.request.SecurityHttpServletRequest;
import io.gitee.mingbaobaba.security.starter.response.SecurityHttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@Import({
        SecurityPropertiesConfiguration.class,
        SecurityBeanConfiguration.class,
        SecurityHttpServletRequest.class,
        SecurityHttpServletResponse.class,
        SecuritySpringContext.class,
        CaptchaEndpoint.class
})
@Slf4j
public class SecurityAutoConfiguration {

    @PostConstruct
    public void postConstruct() {
        log.info("Athena-Security [athena-security-spring-boot-starter] Configure.");
    }

}
