package io.gitee.mingbaobaba.security.oauth2.starter.configuration;

import io.gitee.mingbaobaba.security.oauth2.SecurityOauth2Manager;
import io.gitee.mingbaobaba.security.oauth2.properties.SecurityOauth2Properties;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2ApplicationRepository;
import io.gitee.mingbaobaba.security.oauth2.repository.SecurityOauth2Repository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Objects;

/**
 * <p>bean注入</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/13 7:11
 */
public class SecurityOauth2BeanConfiguration {

    /**
     * 注入配置文件
     *
     * @param securityOauth2Properties SecurityOauth2Properties
     */
    public SecurityOauth2BeanConfiguration(@Autowired(required = false) SecurityOauth2Properties securityOauth2Properties) {
        SecurityOauth2Manager.setConfig(securityOauth2Properties);
    }

    /**
     * oauth2存储
     *
     * @param securityOauth2Repository SecurityOauth2Repository
     */
    @Autowired(required = false)
    public void setSecurityOauth2Repository(SecurityOauth2Repository securityOauth2Repository) {
        if (Objects.nonNull(securityOauth2Repository)) {
            SecurityOauth2Manager.setSecurityOauth2Repository(securityOauth2Repository);
        }
    }
    
    /**
     * 查询应用配置接口
     *
     * @param securityOauth2ApplicationRepository SecurityOauth2ApplicationRepository
     */
    @Autowired(required = false)
    public void setSecurityOauth2ApplicationRepository(SecurityOauth2ApplicationRepository securityOauth2ApplicationRepository) {
        if (Objects.nonNull(securityOauth2ApplicationRepository)) {
            SecurityOauth2Manager.setSecurityOauth2ApplicationRepository(securityOauth2ApplicationRepository);
        }
    }


}
