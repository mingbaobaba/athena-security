package io.gitee.mingbaobaba.security.oauth2.starter.configuration;


import io.gitee.mingbaobaba.security.oauth2.starter.endpoint.SecurityOauth2Endpoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import javax.annotation.PostConstruct;

/**
 * <p>自动配置</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 8:27
 */
@Configuration(proxyBeanMethods = false)
@Import({
        SecurityOauth2PropertiesConfiguration.class,
        SecurityOauth2BeanConfiguration.class,
        SecurityOauth2Endpoint.class
})
@Slf4j
public class SecurityAutoConfiguration {


    @PostConstruct
    public void postConstruct() {
        log.info("Athena-Security [athena-security-oauth2-spring-boot-starter] Configure.");
    }


}
