package io.gitee.mingbaobaba.security.core.listener;

import io.gitee.mingbaobaba.security.core.constants.SecurityConstant;
import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

/**
 * <p>监听打印日志</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 15:17
 */
@Slf4j
public class LoggerSecurityListenerImpl implements SecurityListener {


    @Override
    public void doLogin(String loginId, String token, SecurityLoginParams loginModel) {
        log.info("账号 {} 登录成功,token={},deviceType={}", loginId, token,
                Objects.nonNull(loginModel) ? loginModel.getDeviceType() : SecurityConstant.UNKNOWN);
    }

    @Override
    public void doKickOut(String loginId, String token, String deviceType) {
        log.info("账号 {} 被踢下线,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doReplaceOut(String loginId, String token, String deviceType) {
        log.info("账号 {} 被顶下线,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doBanned(String loginId, String token, String deviceType) {
        log.info("账号 {} 被封禁,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doUnseal(String loginId, String token, String deviceType) {
        log.info("账号 {} 被解封,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doRenewal(String loginId, String token, String deviceType) {
        log.info("账号 {} 被续约,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doRemove(String loginId, String token, String deviceType) {
        log.info("账号 {} 被删除,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doLoginOut(String loginId, String token, String deviceType) {
        log.info("账号 {} 被退出,token={},deviceType={}", loginId, token, deviceType);
    }

    @Override
    public void doCreatedSecuritySession(String securitySessionId) {
        log.info("SecuritySession创建成功,securitySessionId={}", securitySessionId);
    }

    @Override
    public void doDestroySecuritySession(String securitySessionId) {
        log.info("SecuritySession销毁成功,securitySessionId={}", securitySessionId);
    }
}
