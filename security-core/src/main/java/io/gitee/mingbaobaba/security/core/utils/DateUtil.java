package io.gitee.mingbaobaba.security.core.utils;

import org.apache.commons.lang3.time.DateFormatUtils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.function.Function;

/**
 * <p>日期工具</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/28 14:36
 */
public class DateUtil extends DateFormatUtils {

    public static final String FORMAT_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public static final String FORMAT_DATE_PATTERN = "yyyy-MM-dd";

    public static final String FORMAT_TIME_PATTERN = "HH:mm:ss";

    private DateUtil() {

    }

    /**
     * 将日期格式化 （yyyy-MM-dd HH:mm:ss）
     */
    public static final Function<Date, String> formatDateTime = date -> new SimpleDateFormat(FORMAT_DATETIME_PATTERN).format(date);

    /**
     * 将日期格式化 （yyyy-MM-dd HH:mm:ss）
     */
    public static final Function<Date, String> formatDate = date -> new SimpleDateFormat(FORMAT_DATE_PATTERN).format(date);

    /**
     * 将字符串转换为LocalDateTime
     */
    public static final Function<String, LocalDateTime> strToLocalDateTime = str -> {
        DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern(FORMAT_DATETIME_PATTERN);
        return LocalDateTime.parse(str, dateFmt);
    };

    /**
     * 将字符串转换为LocalDate
     */
    public static final Function<String, LocalDate> strToLocalDate = str -> {
        DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern(FORMAT_DATE_PATTERN);
        return LocalDate.parse(str, dateFmt);
    };

    /**
     * 将localDateTime转换为字符串
     */
    public static final Function<LocalDateTime, String> localDateTimeToStr = localDateTime -> {
        DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern(FORMAT_DATETIME_PATTERN);
        return localDateTime.format(dateFmt);
    };

    /**
     * 将localDate转换为字符串
     */
    public static final Function<LocalDate, String> localDateToStr = localDate -> {
        DateTimeFormatter dateFmt = DateTimeFormatter.ofPattern(FORMAT_DATE_PATTERN);
        return localDate.format(dateFmt);
    };
}
