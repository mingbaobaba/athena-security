package io.gitee.mingbaobaba.security.core.context;

import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.domain.SecurityToken;
import io.gitee.mingbaobaba.security.core.request.SecurityRequest;
import io.gitee.mingbaobaba.security.core.response.SecurityResponse;

/**
 * <p>上下文对象</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 13:06
 */
public interface SecurityContext {

    /**
     * SecurityRequest对象
     *
     * @return {@link SecurityRequest}
     */
    SecurityRequest securityRequest();

    /**
     * SecurityResponse对象
     *
     * @return {@link SecurityResponse}
     */
    SecurityResponse securityResponse();

    /**
     * SecuritySession 对象
     *
     * @return {@link SecuritySession}
     */
    SecuritySession securitySession();

    /**
     * SecurityToken 对象
     *
     * @return {@link SecurityToken}
     */
    SecurityToken securityToken();

    /**
     * 路由匹配
     *
     * @param pattern 匹配值
     * @param path    路径
     * @return 是否匹配
     */
    boolean routePathMatch(String pattern, String path);

    /**
     * 是否支持上下文对象
     *
     * @return boolean
     */
    boolean supportContext();
}
