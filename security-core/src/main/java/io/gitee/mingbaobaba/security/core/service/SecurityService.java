package io.gitee.mingbaobaba.security.core.service;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecurityPagination;
import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.enums.SecurityConditionType;
import io.gitee.mingbaobaba.security.core.function.IdentitySwitchFunction;

import java.time.LocalDateTime;
import java.util.List;


/**
 * <p>认证接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 22:09
 */
public interface SecurityService {

    /**
     * 登录方法
     *
     * @param loginId 登录Id
     * @param model   登录参数
     */
    void doLogin(String loginId, SecurityLoginParams model);

    /**
     * 创建指定账号的登录信息
     *
     * @param loginId    登录Id
     * @param loginModel 登录参数
     * @return token
     */
    String createLoginByLoginId(String loginId, SecurityLoginParams loginModel);

    /**
     * 根据token获取SecuritySession信息
     *
     * @param token 用户token
     * @return SecuritySession
     */
    SecuritySession getSecuritySessionByToken(String token);

    /**
     * 根据loginId获取SecuritySession信息
     *
     * @param loginId 用户登录Id
     * @return SecuritySession
     */
    SecuritySession getSecuritySessionByLoginId(String loginId);

    /**
     * 获取当前的SecuritySession信息
     *
     * @return SecuritySession
     */
    SecuritySession getCurrentSecuritySession();

    /**
     * 检查token信息
     */
    void checkToken();

    /**
     * 检查token信息
     *
     * @param token 用户token
     */
    void checkToken(String token);

    /**
     * 踢下线操作
     */
    void kickOut();

    /**
     * 踢下线操作
     *
     * @param token 用户token
     */
    void kickOut(String token);

    /**
     * 顶下线操作
     */
    void replaceOut();

    /**
     * 顶下线操作
     *
     * @param token 用户token
     */
    void replaceOut(String token);

    /**
     * 续约token
     */
    void renewalToken();

    /**
     * 续约token
     *
     * @param token 用户token
     */
    void renewalToken(String token);

    /**
     * 封禁token
     */
    void bannedToken();

    /**
     * 封禁token
     *
     * @param token 用户token
     */
    void bannedToken(String token);

    /**
     * 解封token
     */
    void unsealToken();

    /**
     * 解封token
     *
     * @param token 用户token
     */
    void unsealToken(String token);

    /**
     * 删除token
     *
     * @param token 用户token
     */
    void removeToken(String token);

    /**
     * 退出操作
     */
    void loginOut();

    /**
     * 退出操作
     *
     * @param token 用户token
     */
    void loginOut(String token);

    /**
     * 查询SecuritySession列表
     *
     * @param tokenValue token值，支持模糊匹配
     * @param page       当前页
     * @param limit      每页条数
     * @param sortedDesc 是否降序
     * @return SecurityPagination
     */
    SecurityPagination querySecuritySessionList(String tokenValue, Integer page, Integer limit, Boolean sortedDesc);

    /**
     * 查询token值列表
     *
     * @param tokenValue token值
     * @param sortedDesc 是否降序
     * @return List<String>
     */
    List<String> queryTokenValueList(String tokenValue, Boolean sortedDesc);

    /**
     * 获取session超时时间
     *
     * @param loginId 登录Id
     * @return 时长秒 -1表示永久有效
     */
    Long sessionTimeout(String loginId);

    /**
     * 获取token超时时间
     *
     * @param token tokenValue
     * @return 时长秒 -1表示永久有效
     */
    Long tokenTimeout(String token);

    /**
     * 获取token临时超时时间
     *
     * @param token tokenValue
     * @return 时长秒 -1表示永久有效
     */
    Long tokenActivityTimeout(String token);

    /**
     * 获取token的最新续约时间
     *
     * @param token tokenValue
     * @return 续约时间
     */
    LocalDateTime tokenLastActivityTime(String token);

    /**
     * 当前用户是否有指定角色
     *
     * @param roleCode 角色码
     * @return true 有 false 没有
     */
    Boolean hasRole(String roleCode);

    /**
     * 当前用户是否有指定角色
     *
     * @param roleCode      角色码
     * @param conditionType 条件
     * @return true 有 false 没有
     */
    Boolean hasRole(String[] roleCode, SecurityConditionType conditionType);

    /**
     * 当前用户是否有指定权限码
     *
     * @param permissionCode 权限码
     * @return true 有 false 没有
     */
    Boolean hasPermission(String permissionCode);

    /**
     * 当前用户是否有指定权限码
     *
     * @param permissionCode 权限码
     * @param conditionType  条件
     * @return true 有 false 没有
     */
    Boolean hasPermission(String[] permissionCode, SecurityConditionType conditionType);

    /**
     * 是否已登录
     *
     * @return true 登录 false 未登录
     */
    Boolean isLogin();

    /**
     * 身份临时切换
     *
     * @param loginId            登录Id
     * @param identitySwitchFunc 执行函数
     */
    void identityTempSwitching(String loginId, IdentitySwitchFunction identitySwitchFunc);

    /**
     * token数量统计
     *
     * @return 总数
     */
    Long getTokenCount();
}
