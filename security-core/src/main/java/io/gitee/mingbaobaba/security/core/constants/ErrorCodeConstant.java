package io.gitee.mingbaobaba.security.core.constants;

/**
 * <p>异常码常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 16:02
 */
public interface ErrorCodeConstant {

    /**
     * 无效的监听器
     */
    String CODE_INVALID_LISTENER = "1001";

    /**
     * 配置文件异常
     */
    String CODE_CONFIG_FILE_LOAD_ERR = "1002";

    /**
     * 配置文件属性无法正常读取
     */
    String CODE_CONFIG_FILE_READ_ERR = "1003";

    /**
     * 保存session信息失败
     */
    String CODE_SAVE_SESSION_FAILED = "1004";

    /**
     * 保存token信息失败
     */
    String CODE_SAVE_TOKEN_FAILED = "1005";

    /**
     * 无效的token信息
     */
    String CODE_INVALID_TOKEN = "1006";

    /**
     * 无效的session信息
     */
    String CODE_INVALID_SESSION = "1007";

    /**
     * 被踢下线
     */
    String CODE_KICKED_TOKEN = "1008";

    /**
     * 被顶下线
     */
    String CODE_KNOCKED_TOKEN = "1009";

    /**
     * 被封禁
     */
    String CODE_BANNED_TOKEN = "1010";

    /**
     * token超时
     */
    String CODE_TIMEOUT_TOKEN = "1011";

    /**
     * 无法获取HttpServletRequest
     */
    String CODE_NO_HTTP_SERVLET_REQUEST = "1012";

    /**
     * 无法获取HttpServletResponse
     */
    String CODE_NO_HTTP_SERVLET_RESPONSE = "1013";

    /**
     * 无角色权限
     */
    String CODE_NO_ROLE_ERR = "1014";

    /**
     * 无资源权限
     */
    String CODE_NO_PERMISSION_ERR = "1015";

    /**
     * 不支持路由匹配
     */
    String CODE_NO_SUPPORT_ROUTE_MATCH = "1016";

    /**
     * 不支持上下文
     */
    String CODE_NO_SUPPORT_CONTEXT = "1017";

    /**
     * 无效的参数
     */
    String CODE_INVALID_PARAMS = "1018";

    /**
     * 用户名错误
     */
    String CODE_NO_EXIST_USER = "1019";

    /**
     * 密码错误
     */
    String CODE_PASSWORD_ERR = "1020";

    /**
     * 凭证错误
     */
    String CODE_PARSE_TICKET_ERR = "1021";

    /**
     * 禁用登录
     */
    String CODE_DISABLED_LOGIN = "1022";

    /**
     * 验证码错误
     */
    String CODE_CAPTCHA_ERR = "1023";

    /**
     * 验证版本错误
     */
    String CODE_VERSION_ERR = "1024";

    /**
     * token超过最大颁发限制数
     */
    String CODE_TOKEN_OVERTAKE_MAX_LIMIT = "1025";

}
