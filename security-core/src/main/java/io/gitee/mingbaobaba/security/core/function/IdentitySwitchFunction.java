package io.gitee.mingbaobaba.security.core.function;

/**
 * <p>身份临时切换执行接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/20 16:53
 */
@FunctionalInterface
public interface IdentitySwitchFunction {
    void run();
}
