package io.gitee.mingbaobaba.security.core.domain;

import lombok.Builder;
import lombok.Data;

/**
 * <p>用户信息</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 8:38
 */
@Data
@Builder
public class SecurityUserDetails {

    /**
     * 登录Id
     */
    private String loginId;

    /**
     * 登录用户名
     */
    private String username;

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐值
     */
    private String salt;

}
