package io.gitee.mingbaobaba.security.core.annotion;

import java.lang.annotation.*;

/**
 * <p>忽略认证注解</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/30 16:50
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.METHOD, ElementType.TYPE })
@Documented
public @interface SecurityIgnore {

}
