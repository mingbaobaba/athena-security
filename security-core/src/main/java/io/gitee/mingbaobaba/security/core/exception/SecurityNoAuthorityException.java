package io.gitee.mingbaobaba.security.core.exception;

/**
 * <p>无权限异常</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:05
 */
public class SecurityNoAuthorityException extends SecurityBaseException{

    public SecurityNoAuthorityException(String message) {
        super(message);
    }

    public SecurityNoAuthorityException(String code, String message) {
        super(code, message);
    }
}
