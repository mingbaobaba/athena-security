package io.gitee.mingbaobaba.security.core.repository;

/**
 * <p>验证码存储</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/21 9:19
 */
public interface SecurityCaptchaRepository {

    /**
     * 保存验证码
     *
     * @param code   验证码
     * @param expiry 有效期 单位：秒
     * @return captchaSeqId
     */
    String saveCaptcha(String code, long expiry);

    /**
     * 验证验证码
     *
     * @param captchaSeqId 保存时返回的值
     * @param code         验证码
     * @return 是否正确
     */
    Boolean validCaptcha(String captchaSeqId, String code);

}
