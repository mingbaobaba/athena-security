package io.gitee.mingbaobaba.security.core.listener;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;

/**
 * <p>监听接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 22:05
 */
public interface SecurityListener {

    /**
     * 登录触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param loginModel 登录参数
     */
    void doLogin(String loginId, String token, SecurityLoginParams loginModel);

    /**
     * 被踢下线触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doKickOut(String loginId, String token, String deviceType);

    /**
     * 被顶下线触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doReplaceOut(String loginId, String token, String deviceType);

    /**
     * 被封禁触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doBanned(String loginId, String token, String deviceType);

    /**
     * 解封触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doUnseal(String loginId, String token, String deviceType);

    /**
     * 续约触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doRenewal(String loginId, String token, String deviceType);

    /**
     * 移除触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doRemove(String loginId, String token, String deviceType);

    /**
     * 退出触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    void doLoginOut(String loginId, String token, String deviceType);

    /**
     * 创建 securitySession
     *
     * @param securitySessionId session id
     */
    void doCreatedSecuritySession(String securitySessionId);

    /**
     * 销毁 securitySession
     *
     * @param securitySessionId session id
     */
    void doDestroySecuritySession(String securitySessionId);

}
