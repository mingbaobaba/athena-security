package io.gitee.mingbaobaba.security.core.authority;

import java.util.List;

/**
 * <p>权限认证接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/30 16:15
 */
public interface SecurityAuthority {

    /**
     * 权限码集合
     *
     * @param loginId 登录Id
     * @return List<String>
     */
    List<String> getPermissionCodeList(String loginId);

    /**
     * 角色码集合
     *
     * @param loginId 登录Id
     * @return List<String>
     */
    List<String> getRoleCodeList(String loginId);

}
