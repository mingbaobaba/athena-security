package io.gitee.mingbaobaba.security.core.domain;

import io.gitee.mingbaobaba.security.core.constants.SecurityConstant;
import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>登录参数</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/27 17:15
 */
@Data
public class SecurityLoginParams implements Serializable {

    /**
     * 设备类型
     */
    private String deviceType;

    /**
     * 超时时间
     */
    private Long timeout;

    /**
     * 活跃超时时间
     */
    private Long activityTimeout;

    /**
     * session挂载数据
     */
    private final Map<String, Object> mountData = new HashMap<>();

    /**
     * token挂载数据
     */
    private final Map<String, Object> tokenMountData = new HashMap<>();

    /**
     * 获取设备类型
     *
     * @return 设备类型
     */
    public String getDeviceType() {
        if (Objects.isNull(this.deviceType)) {
            this.deviceType = SecurityConstant.UNKNOWN;
        }
        return deviceType;
    }

    /**
     * 设置设备类型
     *
     * @param deviceType 设备类型
     * @return SecurityLoginParams
     */
    public SecurityLoginParams setDeviceType(String deviceType) {
        this.deviceType = deviceType;
        return this;
    }

    /**
     * 设置超时时间
     *
     * @param timeout 超时时间
     * @return SecurityLoginParams
     */
    public SecurityLoginParams setTimeout(Long timeout) {
        this.timeout = timeout;
        return this;
    }

    /**
     * 设置活跃超时时间
     *
     * @param activityTimeout 活跃超时时间
     * @return SecurityLoginParams
     */
    public SecurityLoginParams setActivityTimeout(Long activityTimeout) {
        this.activityTimeout = activityTimeout;
        return this;
    }

    /**
     * 设置存储信息
     *
     * @param key   key值
     * @param value value值
     * @return SecuritySession
     */
    public SecurityLoginParams setAttribute(String key, Object value) {
        mountData.put(key, value);
        return this;
    }

    /**
     * 获取存储信息
     *
     * @param key key值
     * @return Object
     */
    public Object getAttribute(String key) {
        return mountData.get(key);
    }

    /**
     * 设置token存储信息
     *
     * @param key   key值
     * @param value value值
     * @return SecuritySession
     */
    public SecurityLoginParams setTokenAttribute(String key, Object value) {
        tokenMountData.put(key, value);
        return this;
    }

    /**
     * 获取token存储信息
     *
     * @param key key值
     * @return Object
     */
    public Object getTokenAttribute(String key) {
        return tokenMountData.get(key);
    }
}
