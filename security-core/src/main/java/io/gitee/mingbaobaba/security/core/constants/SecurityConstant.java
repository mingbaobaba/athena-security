package io.gitee.mingbaobaba.security.core.constants;

/**
 * <p>常量</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 22:38
 */
public interface SecurityConstant {

    /**
     * 配置文件地址
     */
    String CONFIG_PATH = "athena-security.properties";

    /**
     * 不过期
     */
    Long NON_EXPIRING = -1L;

    /**
     * 不限制
     */
    Integer NON_LIMIT = -1;

    /**
     * token-正常状态
     */
    String TOKEN_STATE_NORMAL = "1";

    /**
     * token-被踢下线
     */
    String TOKEN_STATE_KICKED_OFFLINE = "2";

    /**
     * token-被顶下线
     */
    String TOKEN_STATE_REPLACE_OFFLINE = "3";

    /**
     * token-封禁
     */
    String TOKEN_STATE_BANNED = "4";

    /**
     * authorization 前缀
     */
    String AUTHORIZATION_PREFIX = "Bearer ";

    /**
     * authorization key
     * {0} token
     */
    String AUTHORIZATION_TOKEN_KEY = AUTHORIZATION_PREFIX + "{0}";

    /**
     * 未知
     */
    String UNKNOWN = "unknown";

    /**
     * 逗号分隔符
     */
    String SPLIT_COMMA = ",";

    /**
     * security session id
     */
    String SECURITY_SESSION_ID = "security_session_id";

    /**
     * 自定义token参数名称
     */
    String SECURITY_CUSTOM_IDENTITY_TOKEN = "security_custom_identity_token";
}
