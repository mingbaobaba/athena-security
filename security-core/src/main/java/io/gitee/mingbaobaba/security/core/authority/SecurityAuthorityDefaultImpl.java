package io.gitee.mingbaobaba.security.core.authority;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>默认权限集合实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/30 16:25
 */
public class SecurityAuthorityDefaultImpl implements SecurityAuthority {
    @Override
    public List<String> getPermissionCodeList(String loginId) {
        return new ArrayList<>();
    }

    @Override
    public List<String> getRoleCodeList(String loginId) {
        return new ArrayList<>();
    }
}
