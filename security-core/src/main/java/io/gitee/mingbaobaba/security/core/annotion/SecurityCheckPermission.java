package io.gitee.mingbaobaba.security.core.annotion;

import io.gitee.mingbaobaba.security.core.enums.SecurityConditionType;

import java.lang.annotation.*;

/**
 * <p>权限检查</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/30 16:54
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
@Documented
public @interface SecurityCheckPermission {

    /**
     * 权限码
     */
    String[] value();

    /**
     * 多个连接条件
     * AND 所有条件都必须满足
     * OR 只要满足任意一个条件
     *
     * @return 条件值
     */
    SecurityConditionType conditionType() default SecurityConditionType.AND;

}
