package io.gitee.mingbaobaba.security.core.response;

/**
 * <p>Response接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/28 20:52
 */
public interface SecurityResponse {

    /**
     * 响应头指定Access-Control-Expose-Headers 否则前端无法读取到这个响应头
     */
    String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";

    /**
     * 获取底层被包装的源对象
     *
     * @return Object
     */
    Object target();

    /**
     * 在响应头里写入一个值
     *
     * @param name  名字
     * @param value 值
     * @return 对象自身
     */
    SecurityResponse setHeader(String name, String value);

    /**
     * 在响应头里添加一个值
     *
     * @param name  名字
     * @param value 值
     * @return 对象自身
     */
    SecurityResponse addHeader(String name, String value);

    /**
     * 添加cookie
     *
     * @param name  名字
     * @param value 值
     * @return SecurityResponse
     */
    SecurityResponse addCookie(String name, String value);

    /**
     * 添加cookie
     *
     * @param name   名字
     * @param value  值
     * @param maxAge 过期的时间（以秒为单位）
     * @return SecurityResponse
     */
    SecurityResponse addCookie(String name, String value, int maxAge);


}
