package io.gitee.mingbaobaba.security.core.domain;

import lombok.Data;

import java.util.List;

/**
 * <p>分页结果</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/9 8:48
 */
@Data
public class SecurityPagination {
    /**
     * 总数
     */
    private long total;

    /**
     * 记录数
     */
    private List<SecuritySession> records;
}
