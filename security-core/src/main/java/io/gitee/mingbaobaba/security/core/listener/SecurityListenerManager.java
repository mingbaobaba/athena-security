package io.gitee.mingbaobaba.security.core.listener;

import io.gitee.mingbaobaba.security.core.constants.ErrorCodeConstant;
import io.gitee.mingbaobaba.security.core.exception.SecurityBusinessException;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>监听器管理</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 15:12
 */
public class SecurityListenerManager {

    private static final List<SecurityListener> LISTENER_LIST = new ArrayList<>();

    private SecurityListenerManager() {

    }

    static {
        SecurityListenerManager.LISTENER_LIST.add(new LoggerSecurityListenerImpl());
    }

    /**
     * 获取所有监听器
     *
     * @return List<SecurityListener>
     */
    public static List<SecurityListener> getListener() {
        return SecurityListenerManager.LISTENER_LIST;
    }

    /**
     * 注册监听器
     *
     * @param listener {@link SecurityListener}
     */
    public static void registerListener(SecurityListener listener) {
        if (null == listener) {
            throw new SecurityBusinessException(ErrorCodeConstant.CODE_INVALID_LISTENER,"注册监听器不能为空");
        }
        LISTENER_LIST.add(listener);
    }

    /**
     * 移除监听器
     *
     * @param listener {@link SecurityListener}
     */
    public static void removeListener(SecurityListener listener) {
        if (null == listener) {
            throw new SecurityBusinessException(ErrorCodeConstant.CODE_INVALID_LISTENER,"移除监听器不能为空");
        }
        LISTENER_LIST.remove(listener);
    }

    /**
     * 清空所有已注册的监听器
     */
    public static void clearListener() {
        LISTENER_LIST.clear();
    }

}
