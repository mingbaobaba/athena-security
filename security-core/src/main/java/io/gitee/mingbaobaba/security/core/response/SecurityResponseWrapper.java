package io.gitee.mingbaobaba.security.core.response;

import io.gitee.mingbaobaba.security.core.exception.SecurityBaseException;

/**
 * <p>返回结果包装接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/8 10:02
 */
public interface SecurityResponseWrapper {

    /**
     * json
     */
    String APPLICATION_JSON_UTF8_VALUE = "application/json;charset=UTF-8";

    /**
     * text
     */
    String TEXT_PLAIN_UTF8_VALUE = "text/plain; charset=UTF-8";

    /**
     * 返回类型
     *
     * @return content-type
     */
    default String contentType() {
        return APPLICATION_JSON_UTF8_VALUE;
    }

    /**
     * 包装结果
     *
     * @param data      数据
     * @param errorFlag 是否是异常标记
     * @param e         异常类
     * @return 结果
     */
    Object wrapper(Object data, Boolean errorFlag, SecurityBaseException e);
}
