package io.gitee.mingbaobaba.security.core.service;

import io.gitee.mingbaobaba.security.core.context.SecurityContext;
import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecurityUserDetails;

/**
 * <p>用户接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 16:21
 */
public interface SecurityUserDetailsService {

    /**
     * 根据登录用户名查询用户信息
     *
     * @param username 用户名
     * @return SecurityUserDetails
     */
    SecurityUserDetails findSecurityUserDetailsByUsername(String username);

    /**
     * 根据登录Id查询用户信息
     *
     * @param loginId 登录Id
     * @return SecurityUserDetails
     */
    SecurityUserDetails findSecurityUserDetailsByLoginId(String loginId);

    /**
     * 登录前处理
     *
     * @param username        用户名
     * @param params          登录参数
     * @param securityContext 上下文对象
     * @return boolean
     */
    boolean preHandle(String username, SecurityLoginParams params, SecurityContext securityContext);

    /**
     * 密码策略
     *
     * @param password        用户输入密码
     * @param userDetails     用户信息
     * @param securityContext 上下文对象
     * @return 转换后的密码
     */
    String passwordPolicy(String password, SecurityUserDetails userDetails, SecurityContext securityContext);

    /**
     * 登录完成后处理
     */
    void afterCompletion();
}
