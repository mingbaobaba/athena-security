package io.gitee.mingbaobaba.security.core.exception;

import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;
import lombok.Getter;

import java.io.Serializable;

/**
 * <p>框架异常基类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 15:52
 */
@Getter
public class SecurityBaseException extends RuntimeException implements Serializable {

    /**
     * 错误码
     */
    private final String code;

    public SecurityBaseException(String message) {
        super(message);
        this.code = null;
        SecurityFactory.getSecurityResponseWrapper.get().wrapper(null, true, this);
    }

    public SecurityBaseException(String message, Throwable cause) {
        super(message, cause);
        this.code = null;
        SecurityFactory.getSecurityResponseWrapper.get().wrapper(null, true, this);
    }

    public SecurityBaseException(String code, String message) {
        super(message);
        this.code = code;
        SecurityFactory.getSecurityResponseWrapper.get().wrapper(null, true, this);
    }

    public SecurityBaseException(String code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        SecurityFactory.getSecurityResponseWrapper.get().wrapper(null, true, this);
    }
}
