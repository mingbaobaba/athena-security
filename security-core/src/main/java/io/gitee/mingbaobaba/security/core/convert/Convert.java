package io.gitee.mingbaobaba.security.core.convert;


/**
 * <p>转换接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/6/1 15:49
 */
@FunctionalInterface
public interface Convert<S, T> {

    /**
     * 转换方法
     *
     * @param source 原始类
     * @return S
     */
    T convert(S source);

    default <U> Convert<S, U> andThen(Convert<? super T, ? extends U> after) {
        return s -> {
            T result = this.convert(s);
            return result != null ? after.convert(result) : null;
        };
    }

}
