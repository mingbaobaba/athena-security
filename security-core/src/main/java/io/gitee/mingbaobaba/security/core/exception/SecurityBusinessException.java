package io.gitee.mingbaobaba.security.core.exception;

/**
 * <p>框架业务通用异常</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 15:55
 */
public class SecurityBusinessException extends SecurityBaseException {
    public SecurityBusinessException(String message) {
        super(message);
    }

    public SecurityBusinessException(String code, String message) {
        super(code, message);
    }

    public SecurityBusinessException(String code, String message, Throwable cause) {
        super(code, message, cause);
    }

    public SecurityBusinessException(String message, Throwable cause) {
        super(message, cause);
    }
}
