package io.gitee.mingbaobaba.security.core.service;

import io.gitee.mingbaobaba.security.core.context.SecurityContext;
import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecurityUserDetails;
import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;

/**
 * <p>用户查询默认实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 16:22
 */
public class SecurityUserDetailsServiceDefaultImpl implements SecurityUserDetailsService {

    @Override
    public SecurityUserDetails findSecurityUserDetailsByUsername(String username) {
        return SecurityUserDetails.builder()
                .loginId(SecurityFactory.getConfig.get().getLoginConfig().getUsername())
                .username(username)
                .name(username)
                .password(SecurityFactory.getConfig.get().getLoginConfig().getPassword())
                .build();
    }

    @Override
    public SecurityUserDetails findSecurityUserDetailsByLoginId(String loginId) {
        return SecurityUserDetails.builder()
                .loginId(loginId)
                .username(SecurityFactory.getConfig.get().getLoginConfig().getUsername())
                .name(SecurityFactory.getConfig.get().getLoginConfig().getUsername())
                .password(SecurityFactory.getConfig.get().getLoginConfig().getPassword())
                .build();
    }

    @Override
    public boolean preHandle(String username, SecurityLoginParams params, SecurityContext securityContext) {
        return username.equals(SecurityFactory.getConfig.get().getLoginConfig().getUsername());
    }

    @Override
    public String passwordPolicy(String password, SecurityUserDetails userDetails, SecurityContext securityContext) {
        return password;
    }

    @Override
    public void afterCompletion() {
    }
}
