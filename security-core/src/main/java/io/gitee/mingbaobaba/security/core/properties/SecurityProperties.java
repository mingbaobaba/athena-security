package io.gitee.mingbaobaba.security.core.properties;

import lombok.Data;

import java.io.Serializable;

/**
 * <p>配置文件</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 21:57
 */
@Data
public class SecurityProperties implements Serializable {

    private static final long serialVersionUID = -1L;

    /**
     * 显示banner
     */
    private Boolean showBanner = true;

    /**
     * 认证名称
     */
    private String securityName = "Athena-Authorization";

    /**
     * 授权有效期（单位：秒） 默认30天，-1 代表永久
     */
    private Long timeout = 60 * 60 * 24 * 30L;

    /**
     * 最低活跃频率（单位：秒），如果 token 超过此时间没有访问系统就会被冻结，，默认30分钟 ，-1 代表不限制，永不冻结
     */
    private Long activityTimeout = 60 * 60L;

    /**
     * 是否自动续约 默认为true 设置为true时会在调用checkToken完成时自动调用续约方法
     */
    private Boolean autoRenewal = true;

    /**
     * 自动续约间隔时长（单位：秒）
     */
    private Long autoRenewalIntervalTime = 180L;

    /**
     * 同一账号，多地同时登录 true表示允许一起登录,false表示新登录会挤掉旧登录
     */
    private Boolean isConcurrentLogin = true;

    /**
     * 同一账号，允许最大登录数量 -1表示不限制 （当isConcurrentLogin为true时此配置项才有效）
     */
    private Integer maxLoginLimit = -1;

    /**
     * 同一账号，允许同时登录的设备类型数量， -1表示不限制
     */
    private Integer maxLoginDeviceTypeLimit = -1;

    /**
     * 颁发token最大限制  -1表示不限制
     */
    private Integer issueTokenMaxLimit = -1;

    /**
     * 是否开启cookie 默认开启
     */
    private Boolean enableCookie = true;

    /**
     * cookie配置
     */
    private CookieProperties cookieConfig = new CookieProperties();

    /**
     * 登录配置
     */
    private LoginProperties loginConfig = new LoginProperties();

    @Data
    public static class CookieProperties implements Serializable {

        /**
         * cookie名称
         */
        private String cookieName = "Athena-Cookie";

        /**
         * 域设置
         */
        private String domain;

        /**
         * 路径设置
         */
        private String path;

        /**
         * 是否应该只在加密的（即 SSL）连接上发送
         */
        private Boolean secure;

        /**
         * 是否禁止 js 操作 Cookie
         */
        private Boolean httpOnly = true;

        /**
         * 该方法规定了描述 cookie 目的的注释。该注释在浏览器向用户呈现 cookie 时非常有用。
         */
        private String comment;

    }

    @Data
    public static class LoginProperties implements Serializable {
        /**
         * 用户名
         */
        private String username = "admin";

        /**
         * 密码
         */
        private String password = "admin@123";

        /**
         * 是否开启验证码
         */
        private boolean captchaEnabled = true;

        /**
         * 图形验证码生成 true 是 false只生成验证码
         */
        private boolean captchaGenerateImage = true;

        /**
         * 登录成功跳转地址
         */
        private String successPage;

        /**
         * 是否禁用登录
         */
        private boolean disabled = false;

        /**
         * 登录标题
         */
        private String loginTitle = "欢迎使用Athena-Security登录";

        /**
         * 版权信息
         */
        private String copyright = "Copyright 2023 Athena-Security. All Rights Reserved";
    }

}
