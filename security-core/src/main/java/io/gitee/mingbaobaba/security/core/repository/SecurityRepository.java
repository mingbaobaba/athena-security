package io.gitee.mingbaobaba.security.core.repository;

import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.domain.SecurityToken;

import java.util.List;

/**
 * <p>持久化接口定义</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 21:55
 */
public interface SecurityRepository {


    //-----------------------security session----------------------------------

    /**
     * 根据登录Id查询SecuritySession
     *
     * @param loginId 登录Id
     * @return SecuritySession
     */
    SecuritySession getSecuritySessionByLoginId(String loginId);

    /**
     * 根据登录Id查询SecuritySession的过期时间
     *
     * @param loginId 登录Id
     * @return 过期时间（秒）
     */
    Long getSessionTimeoutByLoginId(String loginId);

    /**
     * 保存 SecuritySession
     *
     * @param session SecuritySession
     * @return true成功 false失败
     */
    boolean saveSecuritySession(SecuritySession session);

    /**
     * 根据登录Id删除SecuritySession
     *
     * @param loginId 登录Id
     * @return true成功 false失败
     */
    boolean removeSecuritySessionByLoginId(String loginId);


    //-----------------------security token----------------------------------

    /**
     * 根据tokenValue获取SecurityToken
     *
     * @param tokenValue token
     * @return 登录Id
     */
    SecurityToken getSecurityTokenByTokenValue(String tokenValue);

    /**
     * 根据tokenValue获取活跃时间
     *
     * @param tokenValue token
     * @return 活跃时间点
     */
    String getActivityTimeByTokenValue(String tokenValue);

    /**
     * 根据tokenValue获取token超时时间
     *
     * @param tokenValue token
     * @return 超时时间（秒）
     */
    Long getTokenTimeOutByTokenValue(String tokenValue);

    /**
     * 根据tokenValue获取token活跃超时时间
     *
     * @param tokenValue token
     * @return 活跃超时时间（秒）
     */
    Long getTokenActivityTimeOutByTokenValue(String tokenValue);

    /**
     * 保存token信息
     *
     * @param token token信息
     * @return true成功 false失败
     */
    boolean saveToken(SecurityToken token);

    /**
     * 根据tokenValue删除token信息
     *
     * @param tokenValue token
     * @return true成功 false失败
     */
    boolean removeTokenByTokenValue(String tokenValue);

    /**
     * 根据token值续约
     *
     * @param tokenValue  token值
     */
    boolean renewalTokenByTokenValue(String tokenValue);

    /**
     * 查询所有token列表
     *
     * @param tokenValue token值，支持模糊匹配
     * @param sortedDesc 是否降序
     * @return token列表
     */
    List<String> queryTokenList(String tokenValue, boolean sortedDesc);

}
