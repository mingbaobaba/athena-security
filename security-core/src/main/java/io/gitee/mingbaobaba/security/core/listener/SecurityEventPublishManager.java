package io.gitee.mingbaobaba.security.core.listener;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;

/**
 * <p>事件发布管理</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/22 15:24
 */
public class SecurityEventPublishManager {

    private SecurityEventPublishManager() {
    }

    /**
     * 登录接口操作触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param loginModel 登录参数
     */
    public static void doLogin(String loginId, String token, SecurityLoginParams loginModel) {
        SecurityListenerManager.getListener().forEach(item -> item.doLogin(loginId, token, loginModel));
    }

    /**
     * 被踢下线操作触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doKickOut(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doKickOut(loginId, token, deviceType));
    }

    /**
     * 被顶下线操作触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doReplaceOut(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doReplaceOut(loginId, token, deviceType));
    }

    /**
     * 被封禁触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doBanned(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doBanned(loginId, token, deviceType));
    }

    /**
     * 解封触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doUnseal(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doUnseal(loginId, token, deviceType));
    }

    /**
     * 续约触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doRenewal(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doRenewal(loginId, token, deviceType));
    }

    /**
     * 移除触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doRemove(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doRemove(loginId, token, deviceType));
    }

    /**
     * 退出触发
     *
     * @param loginId    登录Id
     * @param token      token值
     * @param deviceType 设备类型
     */
    public static void doLoginOut(String loginId, String token, String deviceType) {
        SecurityListenerManager.getListener().forEach(item -> item.doLoginOut(loginId, token, deviceType));
    }

    /**
     * 创建 securitySession
     *
     * @param securitySessionId session id
     */
    public static void doCreatedSecuritySession(String securitySessionId) {
        SecurityListenerManager.getListener().forEach(item -> item.doCreatedSecuritySession(securitySessionId));
    }

    /**
     * 销毁 securitySession
     *
     * @param securitySessionId session id
     */
    public static void doDestroySecuritySession(String securitySessionId) {
        SecurityListenerManager.getListener().forEach(item -> item.doDestroySecuritySession(securitySessionId));
    }
}
