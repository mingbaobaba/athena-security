package io.gitee.mingbaobaba.security.core.utils;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecurityPagination;
import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.domain.SecurityToken;
import io.gitee.mingbaobaba.security.core.enums.SecurityConditionType;
import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;
import io.gitee.mingbaobaba.security.core.function.IdentitySwitchFunction;
import io.gitee.mingbaobaba.security.core.service.SecurityService;

import java.time.LocalDateTime;
import java.util.function.Supplier;

/**
 * <p>认证工具类</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/21 22:06
 */
public class SecurityUtil {

    private SecurityUtil() {

    }

    /**
     * 获取service
     * <p>
     * 返回 SecurityService
     */
    public static final Supplier<SecurityService> securityService = SecurityFactory.getSecurityService;

    /* ***************************************操作相关方法*************************************** */

    /**
     * 登录操作
     *
     * @param loginId 账号ID
     */
    public static void doLogin(String loginId) {
        doLogin(loginId, new SecurityLoginParams());
    }

    /**
     * 登录操作
     *
     * @param loginId    账号ID
     * @param loginModel 参数
     */
    public static void doLogin(String loginId, SecurityLoginParams loginModel) {
        securityService.get().doLogin(loginId, loginModel);
    }

    /**
     * 创建指定账号的登录信息
     *
     * @param loginId 登录Id
     * @return token
     */
    public static String createLoginByLoginId(String loginId) {
        return createLoginByLoginId(loginId, new SecurityLoginParams());
    }

    /**
     * 创建指定账号的登录信息
     *
     * @param loginId    登录Id
     * @param loginModel 登录参数
     * @return token
     */
    public static String createLoginByLoginId(String loginId, SecurityLoginParams loginModel) {
        return securityService.get().createLoginByLoginId(loginId, loginModel);
    }

    /**
     * 检查token信息
     */
    public static void checkToken() {
        securityService.get().checkToken();
    }

    /**
     * 续约token
     */
    public static void renewalToken() {
        securityService.get().renewalToken();
    }

    /**
     * 退出操作
     */
    public static void loginOut() {
        securityService.get().loginOut();
    }

    /**
     * 指定token退出
     *
     * @param token token
     */
    public static void loginOut(String token) {
        securityService.get().loginOut(token);
    }

    /**
     * 指定token顶下线操作
     *
     * @param token 用户token
     */
    public static void replaceToken(String token) {
        securityService.get().replaceOut(token);
    }

    /**
     * 指定token踢下线操作
     *
     * @param token 用户token
     */
    public static void kickToken(String token) {
        securityService.get().kickOut(token);
    }

    /**
     * 指定token封禁操作
     *
     * @param token 用户token
     */
    public static void bannedToken(String token) {
        securityService.get().bannedToken(token);
    }

    /**
     * 指定token解封操作
     *
     * @param token 用户token
     */
    public static void unsealToken(String token) {
        securityService.get().unsealToken(token);
    }

    /**
     * 指定token移除操作
     *
     * @param token 用户token
     */
    public static void removeToken(String token) {
        securityService.get().removeToken(token);
    }

    /**
     * 身份临时切换
     *
     * @param loginId            登录Id
     * @param identitySwitchFunc 执行函数
     */
    public static void identityTempSwitching(String loginId, IdentitySwitchFunction identitySwitchFunc) {
        securityService.get().identityTempSwitching(loginId, identitySwitchFunc);
    }


    /* ***************************************查询相关方法*************************************** */

    /**
     * 获取当前登录的SecuritySession
     *
     * @return {@link SecuritySession}
     */
    public static SecuritySession getCurrentSecuritySession() {
        return securityService.get().getCurrentSecuritySession();
    }

    /**
     * 获取当前的tokenValue
     *
     * @return token
     */
    public static String getCurrentTokenValue() {
        return getCurrentSecuritySession().getCurrentSecurityToken().getToken();
    }

    /**
     * 获取当前的token信息
     *
     * @return token
     */
    public static SecurityToken getCurrentToken() {
        return getCurrentSecuritySession().getCurrentSecurityToken();
    }

    /**
     * 获取当前的登录Id
     *
     * @return String
     */
    public static String getCurrentLoginId() {
        return getCurrentSecuritySession().getLoginId();
    }

    /**
     * 获取session超时时间
     *
     * @param token token
     * @return 时长秒 -1表示永久有效
     */
    public static Long getSessionTimeout(String token) {
        return securityService.get().sessionTimeout(token);
    }

    /**
     * 获取token超时时间
     *
     * @param token token
     * @return 时长秒 -1表示永久有效
     */
    public static Long getTokenTimeout(String token) {
        return securityService.get().tokenTimeout(token);
    }

    /**
     * 获取指定token活跃超时时间
     *
     * @param token token值
     * @return 时长秒 -1表示永久有效
     */
    public static Long getTokenActivityTimeout(String token) {
        return securityService.get().tokenActivityTimeout(token);
    }

    /**
     * 获取指定token最新续约时间
     *
     * @param token token值
     * @return 续约时间
     */
    public static LocalDateTime getTokenLastActivityTime(String token) {
        return securityService.get().tokenLastActivityTime(token);
    }

    /**
     * 是否已登录
     *
     * @return true 登录 false 未登录
     */
    public static Boolean isLogin() {
        return securityService.get().isLogin();
    }

    /**
     * 当前用户是否有指定角色
     *
     * @param roleCode 角色码
     * @return true 有 false 没有
     */
    public static Boolean hasRole(String roleCode) {
        return securityService.get().hasRole(roleCode);
    }

    /**
     * 当前用户是否有指定角色
     *
     * @param roleCode      角色码
     * @param conditionType 条件
     * @return true 有 false 没有
     */
    public static Boolean hasRole(String[] roleCode, SecurityConditionType conditionType) {
        return securityService.get().hasRole(roleCode, conditionType);
    }

    /**
     * 当前用户是否有指定权限码
     *
     * @param permissionCode 权限码
     * @return true 有 false 没有
     */
    public static Boolean hasPermission(String permissionCode) {
        return securityService.get().hasPermission(permissionCode);
    }

    /**
     * 当前用户是否有指定权限码
     *
     * @param permissionCode 权限码
     * @param conditionType  条件
     * @return true 有 false 没有
     */
    public static Boolean hasPermission(String[] permissionCode, SecurityConditionType conditionType) {
        return securityService.get().hasPermission(permissionCode, conditionType);
    }

    /**
     * 查询SecuritySession列表
     *
     * @param tokenValue token
     * @param page       当前页
     * @param limit      每页条数
     * @param sortedDesc 是否降序
     * @return List<SecuritySession>
     */
    public static SecurityPagination querySecuritySessionList(String tokenValue, int page, int limit, boolean sortedDesc) {
        return securityService.get().querySecuritySessionList(tokenValue, page, limit, sortedDesc);
    }

    /**
     * 查询SecuritySession列表
     *
     * @param page  当前页
     * @param limit 每页条数
     * @return List<SecuritySession>
     */
    public static SecurityPagination querySecuritySessionList(int page, int limit) {
        return securityService.get().querySecuritySessionList(null, page, limit, true);
    }
}
