package io.gitee.mingbaobaba.security.core.enums;

/**
 * <p>条件类型</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/22 8:57
 */
public enum SecurityConditionType {
    AND, OR;
}
