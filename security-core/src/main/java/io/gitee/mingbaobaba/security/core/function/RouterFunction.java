package io.gitee.mingbaobaba.security.core.function;

/**
 * <p>路由接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 11:23
 */
@FunctionalInterface
public interface RouterFunction<T> {

   boolean run(T t);

}
