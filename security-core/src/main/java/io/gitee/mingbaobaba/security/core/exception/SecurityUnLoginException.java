package io.gitee.mingbaobaba.security.core.exception;

/**
 * <p>未登录异常</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/14 4:03
 */
public class SecurityUnLoginException extends SecurityBaseException{
    public SecurityUnLoginException(String message) {
        super(message);
    }

    public SecurityUnLoginException(String code, String message) {
        super(code, message);
    }
}
