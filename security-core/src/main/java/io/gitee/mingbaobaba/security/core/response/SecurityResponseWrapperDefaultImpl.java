package io.gitee.mingbaobaba.security.core.response;

import io.gitee.mingbaobaba.security.core.exception.SecurityBaseException;

/**
 * <p>返回结果包装默认实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/8 10:05
 */
public class SecurityResponseWrapperDefaultImpl implements SecurityResponseWrapper {

    @Override
    public String contentType() {
        return SecurityResponseWrapper.TEXT_PLAIN_UTF8_VALUE;
    }

    @Override
    public Object wrapper(Object data, Boolean errorFlag, SecurityBaseException e) {
        return data;
    }
}
