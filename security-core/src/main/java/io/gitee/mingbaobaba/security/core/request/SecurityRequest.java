package io.gitee.mingbaobaba.security.core.request;

/**
 * <p>Request接口</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/28 20:51
 */
public interface SecurityRequest {

    /**
     * 原始对象
     *
     * @return Object
     */
    Object target();

    /**
     * 从请求头获取
     *
     * @param name name
     * @return value
     */
    String getHeader(String name);

    /**
     * 从参数中获取
     *
     * @param name name
     * @return value
     */
    String getParameter(String name);

    /**
     * 从参数中获取数据，并判断不为空，为空则抛出异常
     *
     * @param name name
     * @return value
     */
    String getParameterNonNull(String name);

    /**
     * 设置参数
     *
     * @param name  name
     * @param value value
     * @return SecurityRequest
     */
    SecurityRequest setAttribute(String name, Object value);

    /**
     * 获取参数值
     *
     * @param name name
     * @return 值
     */
    Object getAttribute(String name);

    /**
     * 移除参数
     *
     * @param name name
     */
    void removeAttribute(String name);

    /**
     * 获取cookie
     *
     * @param name 名字
     * @return String
     */
    String getCookie(String name);

    /**
     * 获取 requestURI
     *
     * @return String
     */
    String getRequestURI();

    /**
     * 获取URL
     *
     * @return String
     */
    String getRequestURL();

    /**
     * ServletPath
     *
     * @return String
     */
    String getServletPath();

    /**
     * 上下文路径
     *
     * @return String
     */
    String getContextPath();

}
