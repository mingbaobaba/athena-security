package io.gitee.mingbaobaba.security.core.context;

import io.gitee.mingbaobaba.security.core.constants.ErrorCodeConstant;
import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.domain.SecurityToken;
import io.gitee.mingbaobaba.security.core.exception.SecurityBusinessException;
import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;
import io.gitee.mingbaobaba.security.core.request.SecurityRequest;
import io.gitee.mingbaobaba.security.core.response.SecurityResponse;
import io.gitee.mingbaobaba.security.core.utils.SecurityUtil;

import java.util.Objects;

/**
 * <p>上下文对象默认实现</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/9/6 13:20
 */
public class SecurityContextDefaultImpl implements SecurityContext {

    @Override
    public SecurityRequest securityRequest() {
        return SecurityFactory.getSecurityRequest.get();
    }

    @Override
    public SecurityResponse securityResponse() {
        return SecurityFactory.getSecurityResponse.get();
    }

    @Override
    public SecuritySession securitySession() {
        return SecurityUtil.getCurrentSecuritySession();
    }

    @Override
    public SecurityToken securityToken() {
        return Objects.nonNull(securitySession()) ? securitySession().getCurrentSecurityToken() : null;
    }

    @Override
    public boolean routePathMatch(String pattern, String path) {
        throw new SecurityBusinessException(ErrorCodeConstant.CODE_NO_SUPPORT_ROUTE_MATCH,"不支持路由匹配,请实现此方法");
    }


    @Override
    public boolean supportContext() {
        return true;
    }
}
