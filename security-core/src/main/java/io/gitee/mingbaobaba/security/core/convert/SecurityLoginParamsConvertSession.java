package io.gitee.mingbaobaba.security.core.convert;

import io.gitee.mingbaobaba.security.core.domain.SecurityLoginParams;
import io.gitee.mingbaobaba.security.core.domain.SecuritySession;
import io.gitee.mingbaobaba.security.core.domain.SecurityToken;
import io.gitee.mingbaobaba.security.core.factory.SecurityFactory;

import java.util.Objects;

/**
 * <p>SecurityLoginModel->SecuritySession</p>
 *
 * @author yingsheng.ye
 * @version 1.0.0
 * @since 2023/8/28 14:10
 */
public class SecurityLoginParamsConvertSession implements Convert<SecurityLoginParams, SecuritySession> {

    private final String loginId;

    private final SecurityToken securityToken;

    public SecurityLoginParamsConvertSession(String loginId, SecurityToken securityToken) {
        this.loginId = loginId;
        this.securityToken = securityToken;
    }

    @Override
    public SecuritySession convert(SecurityLoginParams source) {
        SecurityToken token = Objects.isNull(securityToken) ?
                new SecurityLoginParamsConvertToken(loginId).convert(source) : securityToken;
        SecuritySession session = new SecuritySession(true);
        session.setLoginId(loginId);
        session.getMountData().putAll(source.getMountData());
        session.setTimeout(Objects.isNull(source.getTimeout()) ? SecurityFactory.getConfig.get()
                .getTimeout() : source.getTimeout());
        session.setCurrentSecurityToken(token);
        session.setVersion(0L);
        session.addTokenInfo(token);
        return session;
    }
}
