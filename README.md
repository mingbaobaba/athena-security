<p align="center">
	<img alt="athena-security" src="https://foruda.gitee.com/images/1694696747857673564/88eccf4d_442884.png" width="211" height="211">
</p>

#### 前言
* 更多文档说明 https://gitee.com/mingbaobaba/athena-security/wikis/
* 点个star鼓励一下吧 (*^_^*)

#### athena-security 介绍
athena-security是一个基于token的轻量级权限认证框架，以简单、优雅为设计目标。主要解决登录认证等一系列权限相关问题。

#### 框架设计
![功能架构图](https://foruda.gitee.com/images/1695210764207670195/08d6b7b4_442884.png "架构图.png")

#### 功能使用
* 登录认证
``` java
//会话登录 支持单端登录 多端登录 同端互斥登录
SecurityUtil.doLogin("账号");
```
* 登录检查
``` java
//检查token
SecurityUtil.checkToken();
```
* 登录信息
``` java
//获取登录信息
SecurityUtil.getCurrentSecuritySession();
```
* 账号封禁
``` java
//对指定token进行封禁操作
SecurityUtil.bannedToken("token值");
```
* 账号解封
``` java
//对指定token进行解封操作
SecurityUtil.unsealToken("token值");
```
* 踢人下线
``` java
//对指定token进行踢下线操作
SecurityUtil.kickToken("token值");
```
* 自动续签
``` java
//框架支持自动续约配置，也可以手动调用进行续约操作
SecurityUtil.renewalToken();
```
* 退出登录
``` java
//退出当前登录账号
SecurityUtil.loginOut();
```
* 查询列表
``` java
//查询SecuritySession列表
SecurityUtil.querySecuritySessionList(当前页,分页大小);
```
* 移除操作
``` java
//指定token移除操作
SecurityUtil.removeToken("token值");
```
* 临时身份切换
``` java
//临时身份切换
SecurityUtil.identityTempSwitching("登录Id",()->{
//操作内容
});
```
* 角色权限判断
``` java
//判断是否拥有指定角色 此方法需要实现权限认证接口
SecurityUtil.hasRole("角色码");
```
* 权限码判断
``` java
//判断是否有指定权限码 此方法需要实现权限认证接口
SecurityUtil.hasPermission("权限码");
```
* 权限认证
``` java
//1、实现SecurityAuthority接口，实现getPermissionCodeList、getRoleCodeLis方法
io.gitee.mingbaobaba.security.core.authority.SecurityAuthority

//2、配置拦截器
registry.addInterceptor(new SecurityInterceptor(new SecurityInterceptor(m -> SecurityRouter.build().run(m))))
        .addPathPatterns("/**")
        
//3、添加注解说明
@SecurityIgnore //忽略认证
@SecurityCheckRole //检查角色值
@SecurityCheckPermission //检查权限值       
```
* Oauth2认证

...

#### 基础集成说明
* redis + spring boot 集成
``` java
<dependency>
    <groupId>io.gitee.mingbaobaba</groupId>
    <artifactId>athena-security-plugin-redis</artifactId>
    <version>最新版本</version>
</dependency>
<dependency>
    <groupId>io.gitee.mingbaobaba</groupId>
    <artifactId>athena-security-spring-boot-starter</artifactId>
    <version>最新版本</version>
</dependency>
```

#### 接口访问
``` text
在请求header头中添加 
key：Athena-Authorization
value: token值
```

### 交流QQ群
259890823 进群问题答案 Athena-Security
